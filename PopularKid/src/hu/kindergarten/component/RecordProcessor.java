
package hu.kindergarten.component;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Steindl Kristof 
 */
public class RecordProcessor {
	
	private final String textPath;

	public RecordProcessor(String textPath) {
		this.textPath = textPath;
	}

	public List<Map.Entry<String, Integer>> getMostPopularKid() throws FileNotFoundException, IOException {
		Map<String, Integer> popularitySummary = getPopularitySummary();
		List<Map.Entry<String, Integer>> mostPopularKidEntries = new ArrayList<>();
		for (Map.Entry<String, Integer> entry : popularitySummary.entrySet()) {
			if (mostPopularKidEntries.size() == 0 || entry.getValue() == mostPopularKidEntries.get(0).getValue() ) {
				mostPopularKidEntries.add(entry);
			} else if (entry.getValue() > mostPopularKidEntries.get(0).getValue()) {
				mostPopularKidEntries.clear();
				mostPopularKidEntries.add(entry);
			}
		}
		return mostPopularKidEntries;
	}
		
	private Map<String, Integer> getPopularitySummary() throws FileNotFoundException, IOException {
		Gizi gizi = new Gizi(textPath);
		Map<String, Set<String>> processedLists = gizi.getProcessedLists();
		Map<String, Integer> summary = new HashMap<>();
		processedLists.values().stream().forEach(set -> {
			set.forEach(name -> {
				if (summary.get(name) == null) {
					summary.put(name, new Integer(0));
				}
				Integer number = summary.get(name)+1;
				summary.put(name, number);
			});
		});
		return summary;
	}

		
}

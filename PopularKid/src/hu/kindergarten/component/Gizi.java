
package hu.kindergarten.component;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Steindl Kristof 
 */
public class Gizi {
		
	private final String textPath;

	public Gizi(String textPath) {
		this.textPath = textPath;
	}	
	
	public Map<String, Set<String>> getProcessedLists() throws FileNotFoundException, IOException {
		Map<String, Set<String>> processedLists = new HashMap<>();
		try (BufferedReader bufferedReader = new BufferedReader(new FileReader(textPath))) {
			bufferedReader.lines().forEach(line -> processLine(line, processedLists));
		}
		return processedLists;	
	}
	
	private void processLine(String line, Map<String, Set<String>> processedLists) {
		String[] nameArray = line.split(getSplitterRegex());
		String author = nameArray[0];
		if (processedLists.get(author) == null) {
			processedLists.put(author, new HashSet<>());
		}
		Set<String> newNames = new HashSet<>(Arrays.asList(Arrays.copyOfRange(nameArray, 1, nameArray.length)));
		processedLists.get(author).addAll(newNames);	
	}
	
	private String getSplitterRegex() {
		return " ";
	}
	
	

}


package hu.kindergarten;

import hu.kindergarten.component.RecordProcessor;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Steindl Kristof 
 */
public class App {
	
	private final static String RECORDS_FILE_PATH = "./resources/records.txt";
	
	public static void main(String[] args) {
		String textPath;
		if (args.length > 0) {
			textPath = args[0];
		} else {
			textPath = RECORDS_FILE_PATH;
		}
		RecordProcessor recordProcessor = new RecordProcessor(textPath);
		try {
			List<Map.Entry<String, Integer>> mostPopularKids = recordProcessor.getMostPopularKid();
			System.out.println(mostPopularKids);
		} catch (FileNotFoundException ex) {
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	


}

# UnusedFileManager

The application deletes the unnecessery files and folders in a given root folder.
There's a folder structure where documents are edited by users. 
For each open document there's a .bak file as backup (doc1.bak for doc1.doc), but it's quite common that they are left there after closing the document editor. 
In many cases the documents are also deleted or moved and an empty folder wouldn't be necessary, but they are left there because they contain only unnecessary .bak files. 
The application starting from a root folder given as a program parameter - cleans up the remaining .bak files without a corresponding document and then deletes any 
folder emptied by the clean up process. 
The solution of the application models a situation, where the deletion is a very slow operation.
The root folder is in the project root folder with the name "test_folder".
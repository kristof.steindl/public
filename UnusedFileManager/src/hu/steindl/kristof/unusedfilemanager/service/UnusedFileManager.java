package hu.steindl.kristof.unusedfilemanager.service;


import hu.steindl.kristof.unusedfilemanager.runnable.DeleteRunnable;
import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Steindl Kristof
 */
public class UnusedFileManager {
	
	//Due to specification, the cost of the deletion is orders of magnitude larger, than the iteration of the file system.
	
	public static final String BAK_EXTENSION = "bak";
	public static final int MAX_THREADS = 10;
	
	//This is NOT elegant way, BUT this was the most confortable solution for declaring a reference, that every method in everywhere in the stack sees
	
	private final String rootPath;
	private Boolean shouldRunAgain = true;
	
	private final ExecutorService fileDeletionExecutorService = Executors.newFixedThreadPool(MAX_THREADS);

	public UnusedFileManager(String rootPath) {
		this.rootPath = rootPath;
	}

	public void deleteUnusedFiles() {
		try {
			deleteRecursevelyBakFromFolder(rootPath);
			fileDeletionExecutorService.shutdown();
			fileDeletionExecutorService.awaitTermination(2, TimeUnit.MINUTES);
			int i = 0;
			while (shouldRunAgain) {
				System.out.println("folder delete iteration started: " + i);
				System.out.println("shouldRunAgain: " + shouldRunAgain);
				ExecutorService folderDeletionExecutorService = Executors.newFixedThreadPool(MAX_THREADS);
				shouldRunAgain = false;
				deleteEmptyFoldersRecursevely(rootPath, folderDeletionExecutorService);
				folderDeletionExecutorService.shutdown();
				folderDeletionExecutorService.awaitTermination(2, TimeUnit.MINUTES);			
			}
			
		} catch (InterruptedException ex) {
			Logger.getLogger(UnusedFileManager.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	private void deleteEmptyFoldersRecursevely(String rootFolderPath, ExecutorService folderDeletionExecutorService) {
		File rootFile =  new File(rootFolderPath);
		File[] files = rootFile.listFiles();
		if (files.length == 0) {
			folderDeletionExecutorService.submit(new DeleteRunnable(rootFile));
			shouldRunAgain = true;
			System.out.println("ROOT deletion started, with: " + rootFile.getAbsolutePath());
		}
		for (File file : files) {
			if (file.isDirectory()) {
				deleteEmptyFoldersRecursevely(file.getAbsolutePath(), folderDeletionExecutorService);
			}
		}
	}
	
	private void deleteRecursevelyBakFromFolder(String rootFolderPath) {
		File rootFile =  new File(rootFolderPath);
		File[] files =rootFile.listFiles();
		Set<String> fileNames = new HashSet<>();  
		for (File file : files) {
			if (file.isDirectory()) {
				System.out.println(file.getName());
				deleteRecursevelyBakFromFolder(file.getAbsolutePath());
			}
			else {
				if (!isBakFile(file)) {
					fileNames.add(file.getName().substring(0,file.getName().lastIndexOf('.')));
				}
			}
		}
		System.out.println(fileNames);
		for (File file : files) {
			if (!file.isDirectory()) {
				System.out.println(file.getName());
				boolean isUnnecessesry = !fileNames.contains(file.getName().substring(0,file.getName().lastIndexOf('.')));
				System.out.println(isBakFile(file) + ", " + isUnnecessesry);
				if (isBakFile(file) && isUnnecessesry) {
					fileDeletionExecutorService.submit(new DeleteRunnable(file));
				}				
			}
		}
	}
	
	private boolean isBakFile(File file) {
		return file.getName().substring(file.getName().lastIndexOf('.') + 1).equals(BAK_EXTENSION);
	}
	
}

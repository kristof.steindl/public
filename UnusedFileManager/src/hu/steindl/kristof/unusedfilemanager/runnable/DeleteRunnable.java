package hu.steindl.kristof.unusedfilemanager.runnable;


import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Steindl Kristof
 */
public class DeleteRunnable implements Runnable{

	private File file;

	public DeleteRunnable(File file) {
		this.file = file;
	}
	
	
	
	@Override
	public void run() {
		String filePathName = file.getAbsolutePath();
		System.out.println("file deletion start: " + filePathName);
		file.delete();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException ex) {
			Logger.getLogger(DeleteRunnable.class.getName()).log(Level.SEVERE, null, ex);
		}
		System.out.println("file deletion finished after 2 sec : " + filePathName);
	}
	
}

package hu.steindl.kristof.unusedfilemanager;


import hu.steindl.kristof.unusedfilemanager.service.UnusedFileManager;

/**
 *
 * @author Steindl Kristof
 */
public class UnusedFileApp {
	
	public static String ROOT_PATH = "./test_folder";
	
	public static void main(String[] args) {
		UnusedFileManager unusedFileManager = new UnusedFileManager(ROOT_PATH);
		unusedFileManager.deleteUnusedFiles();
	}
	
}

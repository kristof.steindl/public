package hu.steindl.kristof.synonym.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Steindl Kristof
 */
public class Synonym {
	
	private static final String OUTPUT_FILE_NAME = "output.out";
	
	public static void processInput(String inputFilePath) {
		File inputFile = new File(inputFilePath); 
		BufferedReader bufferedReader; 
		String outputFileName = inputFile.getParentFile().getAbsolutePath().concat("\\").concat(OUTPUT_FILE_NAME);
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
              new FileOutputStream(outputFileName), "utf-8"))){
			bufferedReader = new BufferedReader(new FileReader(inputFile));
			int testCaseCount = Integer.parseInt(bufferedReader.readLine());
			for (int i = 0; i < testCaseCount; i++) {
				processTestCase(writer, bufferedReader);
				writer.write("NEW QUERY\n");
			}
		} catch (FileNotFoundException ex) {
			Logger.getLogger(Synonym.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(Synonym.class.getName()).log(Level.SEVERE, null, ex);
		} catch (NumberFormatException ex) {
			System.out.println("Wrong number format!");
		}
	}
	
		
	public static void printTestFile(String outputFilePath) {
		File file = new File(outputFilePath); 
		BufferedReader bufferedReader; 
		try {
			bufferedReader = new BufferedReader(new FileReader(file));
			String st; 
			while ((st = bufferedReader.readLine()) != null) {
				System.out.println(st);
			}
		} catch (FileNotFoundException ex) {
			Logger.getLogger(Synonym.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(Synonym.class.getName()).log(Level.SEVERE, null, ex);
		}		
	}
	
	private static void processTestCase(Writer writer, BufferedReader bufferedReader) throws IOException {
		List<Set<String>> synonymList = new ArrayList<>();
		String testSynonymCount = bufferedReader.readLine();
		int synonymPairCount = Integer.parseInt(testSynonymCount);			
		for (int j = 0; j < synonymPairCount; j++) {
			String[] words = bufferedReader.readLine().toLowerCase().split(" ");
			appendSynonym(words, synonymList);
		}
		int testPairCount = Integer.parseInt( bufferedReader.readLine());
		for (int j = 0; j < testPairCount; j++) {
			String[] words = bufferedReader.readLine().toLowerCase().split(" ");
			String word1 = words[0];
			String word2 = words[1];
			writer.write(areSynonyms(synonymList, word1, word2) ? "synonyms\n" : "different\n");
		}
	}
	
	private static boolean areSynonyms(List<Set<String>> synonymList, String word1, String word2) {	
		boolean areSynonyms = word1.toLowerCase().equals(word2.toLowerCase());
		int counter = 0;
		while (!areSynonyms && synonymList.size() > counter) {
			Set<String> synonyms = synonymList.get(counter);
			areSynonyms = synonyms.contains(word1) && synonyms.contains(word2);
			counter++;
		}
		return areSynonyms;
	}
	
	private static void appendSynonym(String[] words, List<Set<String>> synonymList) {	
		String word1 = words[0];
		String word2 = words[1];
		System.out.println(word1 + ", " + word2);
		boolean synonymFound = false;
		for (Set<String> synonymSet : synonymList) {
			if (synonymSet.contains(word1)) {
				synonymSet.add(word2);
				synonymFound = true;
			} if (synonymSet.contains(word2)) {
				synonymSet.add(word1);
				synonymFound = true;
			}
		}
		if (!synonymFound) {
			Set<String> synonyms = new HashSet<>();
			synonyms.add(word1);
			synonyms.add(word2);
			synonymList.add(synonyms);
		}
	}

}


package hu.steindl.kristof.synonym;

import hu.steindl.kristof.synonym.service.Synonym;

/**
 *
 * @author Steindl Kristof 
 */
public class SynonymApp {

	public static final String EXAMPLE_INPUT_FILE_PATH = "./example.in";
	public static final String INPUT_FILE_PATH = "./test.in";
	public static final String OUTPUT_FILE_PATH = "./output.out";
	
	public static void main(String[] args) {
		Synonym.processInput(EXAMPLE_INPUT_FILE_PATH);
		Synonym.printTestFile(OUTPUT_FILE_PATH);
	}
	
}

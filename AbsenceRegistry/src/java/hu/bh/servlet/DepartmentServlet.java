
package hu.bh.servlet;

import hu.bh.dto.DepartmentDTO;
import hu.bh.dto.EmployeeDTO;
import hu.bh.ejb.DepartmentBean;
import hu.bh.ejb.EmployeeBean;
import hu.bh.entity.Employee;
import hu.bh.mapper.EmployeeMapper;
import java.io.IOException;
import java.math.BigDecimal;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DepartmentServlet extends HttpServlet {
    
    @EJB
    private DepartmentBean departmentBean;
    
    @EJB
    private EmployeeBean eb;

    @Inject
    private EmployeeMapper empMapper;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        request.setAttribute("employeeList", eb.getAllEmployeeDTOs());
        request.getRequestDispatcher("department_form.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        String departmentName = request.getParameter("department_name");
        String bossId = request.getParameter("employeeId");
        System.out.println(departmentName);
        DepartmentDTO departmentDTO = new DepartmentDTO();
        departmentDTO.setDepartmentName(departmentName);
        departmentDTO.setDepartmentBossId(Integer.parseInt(bossId));
        System.out.println(departmentDTO);
        Integer id = departmentBean.insertDepartmentDTO(departmentDTO);
        System.out.println("dep id: " + id);
        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
    }

}
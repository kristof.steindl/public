
package hu.bh.servlet;

import hu.bh.dto.EmployeeDTO;
import hu.bh.ejb.EmployeeBean;
import hu.bh.ejb.LoginBean;
import hu.bh.exception.InvalidUsernameOrPassword;
import hu.bh.javaBean.LoginJavaBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class LoginServlet extends HttpServlet {

	@EJB
	LoginBean lb;
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		LoginJavaBean logBean = (LoginJavaBean) session.getAttribute("loginJavaBean");
		if (logBean == null) {
			logBean = new LoginJavaBean(0, 0, 0);
			session.setAttribute("loginJavaBean", logBean);
		}
		super.service(req, resp); 
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		LoginJavaBean logBean = (LoginJavaBean) session.getAttribute("loginJavaBean");
		if ("logout".equals( request.getParameter("logaction"))) {
			session.setAttribute("loginJavaBean", new LoginJavaBean());
		}
		request.getRequestDispatcher("WEB-INF/login_jsp.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.setAttribute("failed", new Boolean(false));
		LoginJavaBean logBean = (LoginJavaBean) session.getAttribute("loginJavaBean");		
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		try {
			logBean = lb.getLoginJavaBean(email, password);
			session.setAttribute("loginJavaBean", logBean);
		}
		catch (InvalidUsernameOrPassword ex) {
			request.getSession().setAttribute("message", "Sikertelen bejelentkezés. Rossz felhasználónév/jelszó.");
			request.getRequestDispatcher("WEB-INF/login_error.jsp").forward(request, response);
		}
		response.sendRedirect(request.getContextPath() + "/AbsenceListServlet");
//		request.getRequestDispatcher(request.getContextPath() + "/AbsenceListServlet").forward(request, response);
//		response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
	}

	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

}

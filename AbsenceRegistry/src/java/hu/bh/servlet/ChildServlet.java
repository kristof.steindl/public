/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.servlet;

import hu.bh.dao.ChildDAO;
import hu.bh.dbservice.DBService;
import hu.bh.dto.ChildDTO;
import hu.bh.dto.EmployeeDTO;
import hu.bh.ejb.ChildBean;
import hu.bh.ejb.EmployeeBean;
import hu.bh.entity.Child;
import hu.bh.entity.Employee;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.NotFoundException;

/**
 *
 * @author root
 */
public class ChildServlet extends HttpServlet {

    @EJB
    private ChildBean childBean;

    @EJB
    private EmployeeBean employeeBean;

    private List<EmployeeDTO> parents = new ArrayList<>();

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ChildDTO childDTO = null;
        HttpSession session = request.getSession();
		
        if (null != request.getParameter("id")) {
            try {
                childDTO = childBean.getChildDtoById(Integer.parseInt(request.getParameter("id")));
                session.setAttribute("childDTO", childDTO);
            } catch (NotFoundException ex) {
                request.setAttribute("errorMessage", ex.getMessage());
                response.sendRedirect(response.encodeRedirectURL(request.getHeader("referer")));
            }
        } else {
            session.setAttribute("childId", null);
        }

        System.out.println("Log: " + employeeBean.getAllEmployeeDTOs());
        request.setAttribute("parent", employeeBean.getAllEmployeeDTOs());
		request.setAttribute("contextPath", request.getContextPath());
        request.getRequestDispatcher("WEB-INF/child_form.jsp")
                .forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String redirectTo = response.encodeRedirectURL(request.getRequestURI());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Date birthDate = null;
        HttpSession session = request.getSession();
        if (null != session.getAttribute("childDTO")) {
            Integer id = ((ChildDTO) session.getAttribute("childDTO")).getId();
            session.setAttribute("childDTO", null);
            try {
                ChildDTO cdto = new ChildDTO(
                        id,
                        request.getParameter("child_name"),
                        format.parse(request.getParameter("date_of_birth")),
                        request.getParameter("handicapped").equals("1"),
                        childBean.getEmployeeDTOByID(Integer.parseInt(request.getParameter("parent"))));

                try {
                    childBean.updateChild(cdto);
                    System.out.println("kdfjbgoyjbdf");
                    redirectTo = request.getContextPath() + "/ChildListServlet";
                } catch (NotFoundException notFoundException) {
                    request.setAttribute("errorMessage", notFoundException.getMessage());
                    redirectTo = response.encodeRedirectURL(request.getHeader("referer"));
                }
            } catch (ParseException ex) {
                Logger.getLogger(ChildServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                Integer id = childBean.insertChildDTO(
                        new ChildDTO(
                                request.getParameter("child_name"),
                                format.parse(request.getParameter("date_of_birth")),
                                request.getParameter("handicapped").equals("1"),
                                childBean.getEmployeeDTOByID(Integer.parseInt(request.getParameter("parent")))));
                
            } catch (ParseException ex) {
                Logger.getLogger(ChildServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        response.sendRedirect(redirectTo);

//        String examDate = request.getParameter("date_of_birth").toUpperCase();
//        int year = -1, month = -1, day = -1;
//        try {
//            year = Integer.parseInt(examDate.substring(0, 4));
//            month = Integer.parseInt(examDate.substring(5, 7)) - 1;
//            day = Integer.parseInt(examDate.substring(8, 10));
//        } catch (Exception e) {
//            ;
//        }
//
//        Calendar calendar = new GregorianCalendar();
//        calendar.set(year, month, day);
//        Date date = new Date(calendar.getTimeInMillis());
//        cdto.setBirthDate(date);
//        cdto.setName(request.getParameter("child_name"));
//        cdto.setHandicapped(Boolean.parseBoolean(request.getParameter("handicapped")));
//        cdto.setParent(childBean.getEmployeeDTOByID(Integer.parseInt(request.getParameter("parent"))));
//        Integer id = childBean.insertChildDTO(cdto);
//        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

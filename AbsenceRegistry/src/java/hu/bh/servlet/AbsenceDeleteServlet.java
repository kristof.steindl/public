
package hu.bh.servlet;

import hu.bh.ejb.AbsenceBean;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AbsenceDeleteServlet extends HttpServlet {

	@EJB
	AbsenceBean absenceBean;
	

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect(request.getContextPath() + "/AbsenceListServlet");
		
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			absenceBean.deleteAbsence(Integer.parseInt(request.getParameter("absenceDTOId")));
			response.sendRedirect(request.getContextPath() + "/AbsenceListServlet");
		} catch (IllegalArgumentException ex) {
			String errorMessage = "Sikertelen törlés, nincs ilyen ID-jú absence!";
			request.setAttribute("message", errorMessage);
			request.getRequestDispatcher("WEB-INF/error_jsp.jsp").forward(request, response);
		}
		
	}

	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

}

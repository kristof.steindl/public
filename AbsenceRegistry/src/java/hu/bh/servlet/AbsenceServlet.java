package hu.bh.servlet;

import hu.bh.dto.AbsenceDTO;
import hu.bh.dto.EmployeeDTO;
import hu.bh.ejb.AbsenceBean;
import hu.bh.ejb.EmployeeBean;
import hu.bh.enums.AbsenceStatus;
import hu.bh.enums.AbsenceType;
import hu.bh.exception.InvalidIdException;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import hu.bh.handler.AbsenceCounterBean;
import hu.bh.handler.DateAndCalendarHandler;
import hu.bh.exception.InvalidAbsenceRequest;
import java.io.NotActiveException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpSession;
import javax.ws.rs.NotFoundException;

public class AbsenceServlet extends AuthenticationParentServlet {

    @EJB
	AbsenceBean absenceBean;
	
	@EJB
	EmployeeBean employeeBean; 
	
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {		
		HttpSession session = request.getSession(); 
		String stringId = request.getParameter("id");
		if (stringId != null /*request.getParameter("absenceDTOId") != null*/) {
			try {
				AbsenceDTO absenceDTO = absenceBean.getAbsenceDTOByID(Integer.parseInt(stringId));
				request.setAttribute("absenceDTO", absenceDTO);
				initializeAbsenceForm(request, response);
			}
			catch (NumberFormatException | NotFoundException ex) {
				response.sendRedirect(request.getContextPath() + "/AbsenceListServlet");
			}
		}
		else { 
			session.setAttribute("absenceDTO", null);
			initializeAbsenceForm(request, response);
		}
    }
	
	private void initializeAbsenceForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<AbsenceType> absenceTypeList = new ArrayList<>();
		for (AbsenceType absenceType : AbsenceType.values()) {
			absenceTypeList.add(absenceType);
		}
		request.setAttribute("absenceTypeList", absenceTypeList);
		request.setAttribute("employeeList", employeeBean.getAllEmployeeDTOs());
		request.getRequestDispatcher("WEB-INF/absence_form.jsp").forward(request, response);
	}

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
		AbsenceCounterBean absenceCounterBean = lookupAbsenceCounterBeanBean();
		AbsenceDTO adto = new AbsenceDTO();
		System.out.println(request.getParameter("absenceId") );
		if (!request.getParameter("absenceId").equals("")) {
			adto = absenceBean.getAbsenceDTOByID(Integer.parseInt(request.getParameter("absenceId")));
		}

		try {
			EmployeeDTO edto = employeeBean.getEmployeeDTOById(Integer.parseInt(request.getParameter("employeeId")));
			Date startDate = DateAndCalendarHandler.getParsedDateFromRequest(request.getParameter("startDate"));
			Date endDate = DateAndCalendarHandler.getParsedDateFromRequest(request.getParameter("endDate"));
			if (endDate.compareTo(startDate) < 0) {
				throw new InvalidAbsenceRequest("A távollét dátumának a kezdete nem lehet később, mint a távollét végének a dátuma!");
			}
			adto.setStartDay(startDate);
			adto.setEndDay(endDate);
			adto.setType(AbsenceType.parse(Integer.parseInt(request.getParameter("type"))));
			adto.setEmployee(edto);
//			adto.setAbsenceComment(request.getParameter("comment")); //Egyelőre nem kezel file-t a távollétnyilvántartó
			try {
				absenceCounterBean.validateAbsenceRequest(adto);
				absenceBean.insertAbsenceDto(adto);
				response.sendRedirect(request.getContextPath() + "/AbsenceListServlet");
			} catch (InvalidAbsenceRequest ex) {
				forwardToErrorEndPoint(request, response, ex.getMessage());
			}
		} catch (ParseException ex) {
			String errorMessage = "Rossz dátumformátum!";
			forwardToErrorEndPoint(request, response, errorMessage);			
		} catch (InvalidAbsenceRequest | InvalidIdException ex) {
			forwardToErrorEndPoint(request, response, ex.getMessage());
		} catch (NumberFormatException ex) {
			String errorMessage = "Nincs megjelölve dolgozó vagy nem megfelelő dátum formátum!";
			forwardToErrorEndPoint(request, response, errorMessage);
		}
    }
	
	private void forwardToErrorEndPoint(HttpServletRequest request, HttpServletResponse response, String errorMessage)
        throws ServletException, IOException {
		request.setAttribute("message", errorMessage);
		request.getRequestDispatcher("WEB-INF/error_jsp.jsp").forward(request, response);
	}

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

	private AbsenceCounterBean lookupAbsenceCounterBeanBean() {
		try {
			Context c = new InitialContext();
			return (AbsenceCounterBean) c.lookup("java:global/AbsenceRegistry/AbsenceCounterBean!hu.bh.handler.AbsenceCounterBean");
		} catch (NamingException ne) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
			throw new RuntimeException(ne);
		}
	}

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("delete");
		super.doDelete(req, resp); //To change body of generated methods, choose Tools | Templates.
	}


}

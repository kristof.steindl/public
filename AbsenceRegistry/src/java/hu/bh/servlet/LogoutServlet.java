
package hu.bh.servlet;

import hu.bh.javaBean.LoginJavaBean;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogoutServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		LoginJavaBean logBean = new LoginJavaBean(0, 0, 0);
		session.setAttribute("loginJavaBean", logBean);
		response.sendRedirect(request.getContextPath() + "/LoginServlet");
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
	}

	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

}

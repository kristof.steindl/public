/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.servlet;

import hu.bh.dto.PositionDTO;
import hu.bh.ejb.PositionBean;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.NotFoundException;

/**
 *
 * @author Balyi Zoltan
 */
public class PositionServlet extends HttpServlet {

    @EJB
    private PositionBean positionBean;

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        PositionDTO positionDTO = null;

        request.setAttribute("title", "Munkakör");
        request.setAttribute("contextPath", request.getContextPath());

        HttpSession session = request.getSession();
        if (null != request.getParameter("id")) {
            try {
                positionDTO = positionBean.getPositionDTOById(Integer.parseInt(request.getParameter("id")));
                session.setAttribute("positionId", positionDTO.getId());
            } catch (NotFoundException ex) {
                request.setAttribute("errorMessage", ex.getMessage());
                response.sendRedirect(response.encodeRedirectURL(request.getHeader("referer")));
            }
        } else {
            session.setAttribute("positionId", null);
        }

        request.setAttribute("positionDTO", positionDTO);
        request.getRequestDispatcher("WEB-INF/position_form.jsp")
            .forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        String redirectTo = response.encodeRedirectURL(request.getRequestURI());

        HttpSession session = request.getSession();
        if (null != session.getAttribute("positionId")) {
            Integer id = (Integer) session.getAttribute("positionId");
            session.setAttribute("positionId", null);
            PositionDTO pdto = new PositionDTO(
                id,
                request.getParameter("position_name"),
                request.getParameter("dangerous").equals("1")
            );
            try {
                positionBean.updatePosition(pdto);
                redirectTo = request.getContextPath() + "/PositionListServlet";
            } catch (NotFoundException foundException) {
                request.setAttribute("errorMessage", foundException.getMessage());
                redirectTo = response.encodeRedirectURL(request.getHeader("referer"));
            }
        } else {
            Integer id = positionBean.insertPosition(
                new PositionDTO(
                    request.getParameter("position_name"),
                    request.getParameter("dangerous").equals("1")
                )
            );
        }
        response.sendRedirect(redirectTo); // PRG Pattern
    }

}

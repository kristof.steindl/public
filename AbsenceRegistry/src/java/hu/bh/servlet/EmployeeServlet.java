/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.servlet;

import hu.bh.dto.EmployeeDTO;
import hu.bh.ejb.DepartmentBean;
import hu.bh.ejb.EmployeeBean;
import hu.bh.ejb.PositionBean;
import hu.bh.entity.Department;
import hu.bh.entity.Employee;
import hu.bh.entity.Position;
import hu.bh.exception.InvalidIdException;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.NotFoundException;

/**
 *
 * @author Balyi Zoltan
 */
public class EmployeeServlet extends HttpServlet {

    @EJB
    private EmployeeBean employeeBean;

    @EJB
    private DepartmentBean departmentBean;

    @EJB
    private PositionBean positionBean;

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        EmployeeDTO employeeDTO = null;

        HttpSession session = request.getSession();
        if (null != request.getParameter("id")) {
            try {
                employeeDTO = employeeBean.getEmployeeDTOById(Integer.parseInt(request.getParameter("id")));
                session.setAttribute("employeeId", employeeDTO.getId());
            } catch (NotFoundException ex) {
                request.setAttribute("errorMessage", ex.getMessage());
                response.sendRedirect(response.encodeRedirectURL(request.getHeader("referer")));
            } catch (InvalidIdException ex) {
                Logger.getLogger(EmployeeServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            session.setAttribute("employeeId", null);
        }

        request.setAttribute("employeeDTO", employeeDTO);
        request.setAttribute("contextPath", request.getContextPath());

        request.setAttribute("departments", departmentBean.getAllDepartmentDTO());
        request.setAttribute("positions", positionBean.getAllPositionDTO());
        request.getRequestDispatcher("WEB-INF/employee_form.jsp")
            .forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date birthdate = null;
        Date startOfWork = null;
        Date endOfWork = null;
        Department department = null;
        Position position = null;

        if (null != request.getParameter("birthdate") && !"".equals(request.getParameter("birthdate"))) {
            try {
                birthdate = format.parse(request.getParameter("birthdate"));
            } catch (ParseException ex) {
                Logger.getLogger(EmployeeServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (null != request.getParameter("startOfWork") && !"".equals(request.getParameter("startOfWork"))) {
            try {
                startOfWork = format.parse(request.getParameter("startOfWork"));
            } catch (ParseException ex) {
                Logger.getLogger(EmployeeServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (null != request.getParameter("endOfWork") && !"".equals(request.getParameter("endOfWork"))) {
            try {
                endOfWork = format.parse(request.getParameter("endOfWork"));
            } catch (ParseException ex) {
                Logger.getLogger(EmployeeServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (null != request.getParameter("department") && !"".equals(request.getParameter("department"))) {
            try {
                department = departmentBean.getDepartmentById(new Integer(request.getParameter("department")));
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(EmployeeServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (null != request.getParameter("position") && !"".equals(request.getParameter("position"))) {
            try {
                position = positionBean.getPositionById(new Integer(request.getParameter("position")));
            } catch (NotFoundException ex) {
                Logger.getLogger(EmployeeServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

//        System.out.println("---INFO----");
//        System.out.println(request.getParameter("firstname") + " " + request.getParameter("lastname") + " " + birthdate + " " + false + " " + false + " " + startOfWork + " " + endOfWork + " " + 1 + " " + request.getParameter("email") + " " + request.getParameter("password"));
//        System.out.println("---INFOEND----");
        try {
            Integer id = employeeBean.insertEmployee(
                new Employee(request.getParameter("firstname"), request.getParameter("lastname"), birthdate, request.getParameter("handicapped").equals("1"), request.getParameter("contractStyle").equals("1"), startOfWork, endOfWork, Short.parseShort(request.getParameter("ramainingLastYearVacations")), request.getParameter("email"), request.getParameter("password"), department, position)
            );
        } catch (Exception ex) {
            Logger.getLogger(EmployeeServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
//        System.out.println("empl id: " + id);
        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI())); // PRG Pattern
    }

}

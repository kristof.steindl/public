/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.servlet;

import hu.bh.ejb.EmployeeBean;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Balyi Zoltan
 */
public class EmployeeListServlet extends HttpServlet {

    @EJB
    private EmployeeBean employeeBean;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("title", "Dolgozó lista");
        request.setAttribute("contextPath", request.getContextPath());
        request.setAttribute("employeeList", employeeBean.getAllEmployeeDTOs());

        request.getRequestDispatcher("WEB-INF/employee_list.jsp")
            .forward(request, response);
    }

}

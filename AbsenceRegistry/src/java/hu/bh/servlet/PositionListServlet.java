/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.servlet;

import hu.bh.ejb.PositionBean;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Balyi Zoltan
 */
public class PositionListServlet extends HttpServlet {

    @EJB
    private PositionBean positionBean;

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        request.setAttribute("title", "Munkakörök");
        request.setAttribute("contextPath", request.getContextPath());
        request.setAttribute("positionList", positionBean.getAllPositionDTO());

        request.getRequestDispatcher("WEB-INF/position_list.jsp")
            .forward(request, response);
    }

}

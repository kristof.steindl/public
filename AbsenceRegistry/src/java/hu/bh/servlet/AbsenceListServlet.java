
package hu.bh.servlet;

import hu.bh.dto.PositionDTO;
import hu.bh.ejb.AbsenceBean;
import hu.bh.ejb.PositionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class AbsenceListServlet extends AuthenticationParentServlet {

    @EJB
    private AbsenceBean absenceBean;

	//<jsp:useBean id="absenceDTO" scope="request" class="hu.bh.dto.AbsenceDTO"></jsp:useBean>
	//<c:if test="${absenceDTO != null && (absenceDTO.getId()).equals(item.getId())}"></c:if>
//	            <select name="employeeId" class="form-control" >
//                <c:forEach items="${employeeList}" var="item">
//                    <option <c:if test="${absenceDTO != null && (absenceDTO.getEmployee().getId()).equals(item.getId())}"><c:out value="selected=\"selected\""/></c:if> value="${item.getId()}"><c:out value="${item.toStringToDropDown()}" /></option>
//                </c:forEach>
//            </select>
	
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
//        HttpSession session = request.getSession();
//        if (null != session && null != session.getAttribute("errorMessage")) {
//            request.setAttribute("errorMessage", session.getAttribute("errorMessage"));
//            session.setAttribute("errorMessage", null);
//        }
		
        request.setAttribute("title", "Absence list");
        request.setAttribute("contextPath", request.getContextPath());
        request.setAttribute("absenceList", absenceBean.getAllAbsenceDtoList());
        request.getRequestDispatcher("WEB-INF/absence_list.jsp")
            .forward(request, response);
    }

}

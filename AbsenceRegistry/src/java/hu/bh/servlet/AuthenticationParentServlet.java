
package hu.bh.servlet;

import hu.bh.javaBean.LoginJavaBean;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AuthenticationParentServlet extends HttpServlet {

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		LoginJavaBean logBean = (LoginJavaBean) session.getAttribute("loginJavaBean");
		req.setAttribute("contextPath", req.getContextPath());
		if (logBean == null || logBean.getEmployeeId()==0) {
			resp.sendRedirect(req.getContextPath() + "/LoginServlet");
		} 
		else {
			super.service(req, resp); //To change body of generated methods, choose Tools | Templates.
		}
	}


	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

}

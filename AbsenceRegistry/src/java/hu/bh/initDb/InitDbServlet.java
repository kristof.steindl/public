/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.initDb;

import hu.bh.dto.EmployeeDTO;
import hu.bh.ejb.EmployeeBean;
import hu.bh.entity.Absence;
import hu.bh.entity.Child;
import hu.bh.entity.Department;
import hu.bh.entity.Employee;
import hu.bh.entity.Position;
import hu.bh.fillers.DbInitializerBean;
import hu.bh.fillers.TableFillerApp;
import hu.bh.handler.AbsenceCounterBean;
import hu.bh.handler.DateAndCalendarHandler;
import hu.bh.handler.WorkDayCounterBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Emma
 */
public class InitDbServlet extends HttpServlet {

	@EJB
	EmployeeBean eb;
	
	@EJB
	WorkDayCounterBean wdcb;

	@EJB
	DbInitializerBean dbb;
	

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
//		List<EmployeeDTO> employeeList = eb.getAllEmployeeDTOs();
//		if (employeeList.size() < 3) {
			dbb.initDb();
//		}
		
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
	}

	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

}

package hu.bh.ejb;

import hu.bh.dto.PositionDTO;
import hu.bh.entity.Position;
import hu.bh.mapper.PositionMapper;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.NotFoundException;

@Stateless
@LocalBean
public class PositionBean {

    @Inject
    PositionMapper positionMapper;

    @PersistenceContext(unitName = "AbsenceRegistryPU")
    private EntityManager em;

    public Integer insertPosition(PositionDTO pdto) {
        Position p = positionMapper.transformDTO(pdto);
        em.persist(p);
        return p.getId();
    }

    public Position updatePosition(PositionDTO pdto) throws NotFoundException {
        Position p = getPositionById(pdto.getId());
        return em.merge(positionMapper.transformDTO(pdto));
    }

    public List<PositionDTO> getAllPositionDTO() {
        List<PositionDTO> positionDTOs = new ArrayList<>();
        Query query = em.createNamedQuery("Position.findAll", Position.class);
        List<Position> positions = query.getResultList();
        if (!positions.isEmpty()) {
            for (Position position : positions) {
                positionDTOs.add(positionMapper.transformEntity(position));
            }
        }
        return positionDTOs;
    }

    public PositionDTO getPositionDTOById(Integer id) throws NotFoundException {
        return positionMapper.transformEntity(
            getPositionById(id)
        );
    }

    public Position getPositionById(Integer id) throws NotFoundException {
        Query query = em.createNamedQuery("Position.findById", Position.class);
        query.setParameter("id", id);
        Position position = (Position) query.getSingleResult();
        if (null == position) {
            throw new NotFoundException("Position not found.");
        }
        return position;
    }
}

package hu.bh.ejb;

import hu.bh.dto.DepartmentDTO;
import hu.bh.entity.Department;
import hu.bh.mapper.DepartmentMapper;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@LocalBean
public class DepartmentBean {

    @Inject
    DepartmentMapper dm;

    @PersistenceContext(unitName = "AbsenceRegistryPU")
    private EntityManager em;

    public List<Department> getAllDepartment() {
        Query query = em.createNamedQuery("Department.findAll", Department.class);
        return (List<Department>) query.getResultList();
    }

    public List<DepartmentDTO> getAllDepartmentDTO() {
        List<DepartmentDTO> departmentDTOs = new ArrayList<>();
        List<Department> departments = this.getAllDepartment();
        if (!departments.isEmpty()) {
            for (Department department : departments) {
                departmentDTOs.add(dm.transformEntity(department));
            }
        }
        return departmentDTOs;
    }

    public Department getDepartmentById(Integer id) {
        Department employee = em.find(Department.class, id);
        return employee;
    }

    public Integer insertDepartmentDTO(DepartmentDTO ddto) {
        DepartmentMapper dm = new DepartmentMapper();
        Department d = dm.transformDTO(ddto);
        em.persist(d);
        return d.getId();

    }
}

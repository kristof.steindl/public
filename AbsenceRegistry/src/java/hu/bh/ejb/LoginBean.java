
package hu.bh.ejb;

import hu.bh.entity.Employee;
import hu.bh.exception.InvalidUsernameOrPassword;
import hu.bh.javaBean.LoginJavaBean;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;
import javax.ejb.TransactionRolledbackLocalException;
import javax.persistence.NoResultException;


@Stateless
public class LoginBean {
	
	@EJB
	EmployeeBean eb;
	
	public LoginJavaBean getLoginJavaBean(String email, String password) throws InvalidUsernameOrPassword{
		LoginJavaBean logBean = new LoginJavaBean();
		try {
			Employee employee = eb.getEmployeeByEmailAndPassword(email, password);
			logBean.setEmployeeId(employee.getId().intValue());
			logBean.setPositionId(employee.getPosition().getId());
			logBean.setDepartmentId(employee.getDepartment().getId());
		}
		catch (EJBTransactionRolledbackException ex) {
			throw new InvalidUsernameOrPassword();
		}
		return logBean;
	}
	
	
}

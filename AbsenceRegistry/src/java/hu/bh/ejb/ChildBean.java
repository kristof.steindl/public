/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.ejb;

import hu.bh.dto.ChildDTO;
import hu.bh.dto.EmployeeDTO;
import hu.bh.entity.Child;
import hu.bh.entity.Employee;
import hu.bh.mapper.ChildMapper;
import hu.bh.mapper.EmployeeMapper;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.NotFoundException;
import org.omg.CosNaming.NamingContextPackage.NotFound;

/**
 *
 * @author Klara
 */
@Stateless
@LocalBean
public class ChildBean {

    @Inject
    ChildMapper childMapper;

    @Inject
    EmployeeMapper employeeMapper;

    @PersistenceContext(unitName = "AbsenceRegistryPU")
    private EntityManager em;

    public Integer insertChildDTO(ChildDTO cdto) {
        Child c = new Child();        
        if(null!=cdto.getId()){
            //updateChild(cdto);
//            c = childMapper.transformDTO(cdto);
//            em.merge(cdto);
        }else{
            c = childMapper.transformDTO(cdto);
            em.persist(c);
        }
        return c.getId();
    }
    
    public Child updateChild(ChildDTO pdto) throws NotFoundException {
        Child p = findChildById(pdto.getId());
        return em.merge(childMapper.transformDTO(pdto));
    }

    public Child findChildById(int id) {
        return em.find(Child.class, id);
    }

    public EmployeeDTO getEmployeeDTOByID(int id) {

        System.out.println(em.find(Employee.class, new Integer(id)));

        EmployeeDTO edto = employeeMapper.transformEntity(em.find(Employee.class, new Integer(id)));
        return edto;
    }

    public List<ChildDTO> getChildrenDTO() {
        List<ChildDTO> childDTOs = new ArrayList<>();
        Query query = em.createNamedQuery("Child.findAll", Child.class);
        List<Child> children = query.getResultList();
        if (!children.isEmpty()) {
            for (Child child : children) {
                childDTOs.add(childMapper.transformEntity(child));
            }
        }

        return childDTOs;

    }
    
    public ChildDTO getChildDtoById(int id)throws NotFoundException{
        Query query = em.createNamedQuery("Child.findById", Child.class);
        query.setParameter("id", id);
        Child child = (Child) query.getSingleResult();
        if (null==child) {
            throw new NotFoundException("Child not found");
        }
        return childMapper.transformEntity(child);
    }

    public List<Child> getChildListByParent(Employee parent) {
        List<Child> childList = new ArrayList<>();
        TypedQuery<Child> query = em.createQuery(
                "SELECT c FROM Child c WHERE c.parent=:parent",
                Child.class)
                .setParameter("parent", parent);
        try {

            childList = query.getResultList();
        } catch (javax.ejb.EJBTransactionRolledbackException ex) {
            ; //Exception will not be thrown over, because an empty childList is a valid scenario
        }
        return childList;
    }

    public List<ChildDTO> getChildDtoListByEmployeeDto(EmployeeDTO employeeDTO) {
        List<Child> childList = getChildListByParent(employeeMapper.transformDTO(employeeDTO));
        List<ChildDTO> childDTOList = new ArrayList<>();
        for (Child child : childList) {
            childDTOList.add(childMapper.transformEntity(child));
        }
        return childDTOList;
    }

//    public Integer updateChild(ChildDTO cdto) {
//        return em.createQuery("UPDATE Child c SET "
//                + "c.name =:name, "
//                + "c.birthDate =:birthDate, "
//                + "c.handicapped =:handicapped, "
//                + "c.parent =:parent,"
//                + "WERE c.id =:id", Child.class)
//                .setParameter("name", cdto.getName())
//                .setParameter("birthDate", cdto.getBirthDate())
//                .setParameter("handicapped", cdto.isHandicapped())
//                .setParameter("parent", cdto.getParent())
//                .executeUpdate();
//    }

    public void deleteChild(int id) {
        Child child = findChildById(id);
        em.remove(child);
    }

}

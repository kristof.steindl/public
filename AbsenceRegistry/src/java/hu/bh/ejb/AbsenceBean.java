package hu.bh.ejb;

import hu.bh.dto.AbsenceDTO;
import hu.bh.dto.EmployeeDTO;
import hu.bh.entity.Absence;
import hu.bh.entity.Employee;
import hu.bh.handler.AbsenceCounterBean;
import hu.bh.exception.InvalidAbsenceRequest;
import hu.bh.handler.WorkDayCounterBean;
import hu.bh.mapper.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.NotFoundException;

@Stateless
@LocalBean
public class AbsenceBean {

	@Inject
    AbsenceMapper absenceMapper;
	
	@Inject
	EmployeeMapper employeeMapper;
	
	@EJB
	WorkDayCounterBean workDayCounterBean;

    @PersistenceContext(unitName = "AbsenceRegistryPU")
    private EntityManager em;

    public Integer insertAbsenceDto(AbsenceDTO adto) {
        Absence absence = absenceMapper.transformDTO(adto);
		if (absence.getId() == null) {
			em.persist(absence);
		}
		else {
			em.merge(absence);
		}      
        return absence.getId();
    }

	
	public List<Absence> getAbsenceListByEmployee(Employee employee) {
		List<Absence> absenceList = new ArrayList<>();
		TypedQuery<Absence> query = em.createQuery(
				"SELECT a FROM Absence a WHERE a.employee=:employee",
				Absence.class)
				.setParameter("employee", employee);
		try {
			absenceList = query.getResultList();
		} catch (javax.ejb.EJBTransactionRolledbackException ex) {
			; //Exception will not be thrown over, because an empty List is a valid scenario
		}
		return absenceList;
	}
	
	public List<AbsenceDTO> getAbsenceDtoListByEmployeeDto(EmployeeDTO employeeDTO) {
		Employee employee = employeeMapper.transformDTO(employeeDTO);
		List<Absence> absenceList = getAbsenceListByEmployee(employee);
		List<AbsenceDTO> absenceDTOList = new ArrayList<>();
		for (Absence absence : absenceList) {
			absenceDTOList.add(absenceMapper.transformEntity(absence));
		}	
		return absenceDTOList;
	}
	
	public List<AbsenceDTO> getAllAbsenceDtoList() {
		List<AbsenceDTO> absenceDTOList = new ArrayList<>();
		Query query = em.createNamedQuery("Absence.findAll", Absence.class);
        List<Absence> absenceList = query.getResultList();
        if (!absenceList.isEmpty()) {
            for (Absence absence : absenceList) {
                absenceDTOList.add(absenceMapper.transformEntity(absence));
            }
        }
		return absenceDTOList;
	}
	
	public AbsenceDTO getAbsenceDTOByID(Integer id) throws NotFoundException{
		AbsenceDTO absenceDTO = new AbsenceDTO();
		Absence absence = em.find(Absence.class, id);
		if (null == absenceDTO) {
			throw new NotFoundException();
		}
		absenceDTO = absenceMapper.transformEntity(absence);
		return absenceDTO;
	}
	
	public void deleteAbsence(Integer id) throws IllegalArgumentException{
		Absence absence = em.find(Absence.class, id);
		em.remove(absence);
	}
}

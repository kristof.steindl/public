package hu.bh.ejb;

import hu.bh.dto.EmployeeDTO;
import hu.bh.entity.Employee;
import hu.bh.exception.InvalidIdException;
import hu.bh.mapper.AbsenceMapper;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import hu.bh.mapper.EmployeeMapper;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.LocalBean;
import javax.ejb.TransactionRolledbackLocalException;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

@Stateless
public class EmployeeBean {

    @Inject
    EmployeeMapper employeeMapper;

    @PersistenceContext(unitName = "AbsenceRegistryPU")
    private EntityManager entityManager;

    public List<Employee> getAllEmployees() {
        Query query = entityManager.createNamedQuery("Employee.findAll", Employee.class);
        return (List<Employee>) query.getResultList();
    }

    public Employee getEmployeeById(Integer id) throws InvalidIdException {
        Employee employee = entityManager.find(Employee.class, id);
		if (employee == null) {
			throw new InvalidIdException("Employee id is invalid");
		}
        return employee;
    }

    public Integer insertEmployee(Employee e) {
        entityManager.persist(e);
        return e.getId();
    }

	public List<EmployeeDTO> getAllEmployeeDTOs(){
		List<Employee> employeeList = getAllEmployees();
		List<EmployeeDTO> edtoList = new ArrayList<>();
		for (Employee employee : employeeList) {
			edtoList.add(employeeMapper.transformEntity(employee));
		}
		return edtoList;
	}	
	
	public EmployeeDTO getEmployeeDTOById(Integer id) throws InvalidIdException {
		Employee employee = getEmployeeById(id);
		EmployeeDTO edto = employeeMapper.transformEntity(employee);
		return edto;
	}
	
	public Employee getEmployeeByEmailAndPassword(String email, String password) throws EJBTransactionRolledbackException{
		TypedQuery<Employee> query = entityManager.createQuery(
				"SELECT e FROM Employee e WHERE e.email=:email AND e.password=:password",
				Employee.class)
				.setParameter("password", password)
				.setParameter("email", email);
		Employee employee = query.getSingleResult();
		return employee;
	}
//	
//	public EmployeeDTO getEmployeeDTOByEmail(String email) {	
//		Employee employee = getEmployeeByEmail(email);		
//		EmployeeDTO edto = mapper.transformEntity(employee);
//		return edto;
//	}
}

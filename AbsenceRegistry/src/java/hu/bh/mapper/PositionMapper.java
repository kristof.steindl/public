package hu.bh.mapper;

import hu.bh.dto.PositionDTO;
import hu.bh.entity.Position;
import javax.inject.Singleton;

@Singleton
public class PositionMapper implements MapperInterface<Position, PositionDTO> {

    @Override
    public Position transformDTO(PositionDTO d) {
        Position position = new Position();
        position.setId(d.getId());
        position.setPositionName(d.getPositionName());
        position.setDangerous(d.isDangerous());
        return position;
    }

    @Override
    public PositionDTO transformEntity(Position e) {
        PositionDTO positionDTO = new PositionDTO();
        positionDTO.setId(e.getId());
        positionDTO.setPositionName(e.getPositionName());
        positionDTO.setDangerous(e.isDangerous());
        return positionDTO;
    }

}

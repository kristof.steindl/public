/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.mapper;

import hu.bh.dto.ChildDTO;
import hu.bh.entity.Child;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 *
 * @author Klara
 */
@Singleton
public class ChildMapper implements MapperInterface<Child, ChildDTO>{
    
    @Inject
    EmployeeMapper employeeMapper;
    
    
    @Override
    public Child transformDTO(ChildDTO cdto) {
        Child child = new Child();
        child.setName(cdto.getName());
        child.setBirthDate(cdto.getBirthDate());
        child.setHandicapped(cdto.isHandicapped());
        child.setId(cdto.getId());
        child.setParent(employeeMapper.transformDTO(cdto.getParent()));
        return child;
    }

    @Override
    public ChildDTO transformEntity(Child child) {
        ChildDTO cdto = new ChildDTO();
        cdto.setName(child.getName());
        cdto.setBirthDate(child.getBirthDate());
        cdto.setHandicapped(child.isHandicapped());
        cdto.setId(child.getId());
        if (child.getParent()!=null){
            cdto.setParent(employeeMapper.transformEntity(child.getParent()));
        }
        return cdto;
    }
    
}

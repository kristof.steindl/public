/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.mapper;

/**
 *
 * @author Balyi Zoltan
 * @param <E>
 * @param <D>
 */
public interface MapperInterface<E, D> {

    public E transformDTO(D d);

    public D transformEntity(E e);

}

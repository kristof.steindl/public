
package hu.bh.mapper;

import hu.bh.entity.*;
import hu.bh.dto.*;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AbsenceMapper implements MapperInterface<Absence, AbsenceDTO>{

	@Inject
	EmployeeMapper employeeMapper;
	
	@Override
	public Absence transformDTO(AbsenceDTO adto) {
		Absence absence = new Absence();
		absence.setId(adto.getId());
		absence.setStartDay(adto.getStartDay());
		absence.setEndDay(adto.getEndDay());
		absence.setStatus(adto.getStatus());
		absence.setType(adto.getType());
		absence.setAbsenceComment(adto.getAbsenceComment());
		absence.setEmployee(employeeMapper.transformDTO(adto.getEmployee()));
		return absence;
	}

	@Override
	public AbsenceDTO transformEntity(Absence absence) {
		AbsenceDTO adto = new AbsenceDTO();
		adto.setId(absence.getId());
		adto.setStartDay(absence.getStartDay());
		adto.setEndDay(absence.getEndDay());
		adto.setStatus(absence.getStatus());
		adto.setType(absence.getType());
		adto.setAbsenceComment(absence.getAbsenceComment());
		adto.setEmployee(employeeMapper.transformEntity(absence.getEmployee()));
		return adto;
	}


}

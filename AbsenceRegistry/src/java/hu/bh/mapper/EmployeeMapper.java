package hu.bh.mapper;

import hu.bh.dto.EmployeeDTO;
import hu.bh.entity.Employee;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class EmployeeMapper implements MapperInterface<Employee, EmployeeDTO> {

    @Inject
    DepartmentMapper departmentMapper;

    @Inject
    PositionMapper positionMapper;

    @Override
    public Employee transformDTO(EmployeeDTO edto) {
        Employee employee = new Employee();

        employee.setId(edto.getId());
        employee.setFirstname(edto.getFirstname());
        employee.setLastname(edto.getLastname());
        employee.setBirthdate(edto.getBirthdate());
        employee.setContractStyle(edto.isContractStyle());
        employee.setStartOfWork(edto.getStartOfWork());
        employee.setEndOfWork(edto.getEndOfWork());
        employee.setRamainingLastYearVacations(edto.getRamainingLastYearVacations());
        employee.setEmail(edto.getEmail());
        employee.setPassword(edto.getPassword());
		if (edto.getPosition() != null) {
			employee.setPosition(positionMapper.transformDTO(edto.getPosition()));
		}
        if (edto.getDepartment() != null) {
			employee.setDepartment(departmentMapper.transformDTO(edto.getDepartment()));
		}
   

        return employee;
    }

    @Override
    public EmployeeDTO transformEntity(Employee employee) {
        EmployeeDTO edto = new EmployeeDTO();

        edto.setId(employee.getId());
        edto.setFirstname(employee.getFirstname());
        edto.setLastname(employee.getLastname());
        edto.setBirthdate(employee.getBirthdate());
        edto.setContractStyle(employee.isContractStyle());
        edto.setStartOfWork(employee.getStartOfWork());
        edto.setEndOfWork(employee.getEndOfWork());
        edto.setRamainingLastYearVacations(employee.getRamainingLastYearVacations());
        edto.setEmail(employee.getEmail());
        edto.setPassword(employee.getPassword());
		if (employee.getPosition() != null) {
			edto.setPosition(positionMapper.transformEntity(employee.getPosition()));
		}
        if (employee.getDepartment() != null) {
			edto.setDepartment(departmentMapper.transformEntity(employee.getDepartment()));
		}
        return edto;
    }

}

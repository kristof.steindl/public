package hu.bh.mapper;

import hu.bh.dto.DepartmentDTO;
import hu.bh.dto.EmployeeDTO;
import hu.bh.ejb.EmployeeBean;
import hu.bh.entity.Department;
import hu.bh.entity.Employee;
import hu.bh.exception.InvalidIdException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DepartmentMapper implements MapperInterface<Department, DepartmentDTO> {

    @EJB
    EmployeeBean employeeBean;

    @Override
    public Department transformDTO(DepartmentDTO ddto) {
        Department department = new Department();
        department.setId(ddto.getId());
        department.setDepartmentName(ddto.getDepartmentName());
        if (null != ddto.getDepartmentBossId()) {
			try {
				department.setDepartmentBoss(employeeBean.getEmployeeById(ddto.getDepartmentBossId()));
			} catch (InvalidIdException ex) {
				Logger.getLogger(DepartmentMapper.class.getName()).log(Level.SEVERE, null, ex);
			}
        }
        return department;
    }

    @Override
    public DepartmentDTO transformEntity(Department department) {
        DepartmentDTO ddto = new DepartmentDTO();
        ddto.setDepartmentName(department.getDepartmentName());
        ddto.setId(department.getId());
        if (department.getDepartmentBoss() != null) {
            ddto.setDepartmentBossId(department.getDepartmentBoss().getId());
        }
        return ddto;

    }

}

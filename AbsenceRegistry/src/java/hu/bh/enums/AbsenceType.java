
package hu.bh.enums;

public enum AbsenceType {
	GENERAL(1, "Általános"),
	MATERNITY(2, "Szülési"),
	SICKLEAVE(3, "Betegszabadság"),
	OTHER(4, "Egyéb"),
	UNPAIDLEAVE(5, "Fizetetlen"),
	STUDY(6, "Tanulmányi"),
	HOMEOFFICE(7, "Home Office"),
	SECONDMENT(8, "Kiküldetés"),
	BUSINESSTRIP(9, "Üzleti út");

	private int value;
	private String hunString;
	
	AbsenceType(int value, String hunString) {
		this.value = value;
		this.hunString = hunString;
	}
	
	public int getValue() {
		return value; 
	}
	
	public static AbsenceType parse(int id) {
		AbsenceType absenceType = null; // Default
		for (AbsenceType item : AbsenceType.values()) {
			if (item.getValue()==id) {
				absenceType = item;
				break;
			}
		}
		return absenceType;
	}
	
	public String toHunString() {
		return this.hunString;
	}
	
	
}


package hu.bh.enums;

public enum AbsenceStatus {
	PENDING(1, "Függő"),
	REJECTED(2, "Elutasított"),
	APPROVED(3, "Elfogadott");

	private int value;
	private String hunString;

		
	AbsenceStatus(int value, String hunString) {
		this.value = value;
		this.hunString = hunString;
	}
	
	public int getValue() {
		return value; 
	}
	
	public static AbsenceStatus parse(int id) {
		AbsenceStatus absenceStatus = null; // Default
		for (AbsenceStatus item : AbsenceStatus.values()) {
			if (item.getValue()==id) {
				absenceStatus = item;
				break;
			}
		}
		return absenceStatus;
	}
	
	public String toHunString() {
		return this.hunString;
	}
	
}

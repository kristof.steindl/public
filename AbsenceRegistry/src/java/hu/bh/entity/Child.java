/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Balyi Zoltan
 */
@Entity
@Table(name = "CHILD")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Child.findAll", query = "SELECT c FROM Child c")
    , @NamedQuery(name = "Child.findById", query = "SELECT c FROM Child c WHERE c.id = :id")
    , @NamedQuery(name = "Child.findByName", query = "SELECT c FROM Child c WHERE c.name = :name")
    , @NamedQuery(name = "Child.findByBirthDate", query = "SELECT c FROM Child c WHERE c.birthDate = :birthDate")
    , @NamedQuery(name = "Child.findByHandicapped", query = "SELECT c FROM Child c WHERE c.handicapped = :handicapped")})
public class Child implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @Column(name = "BIRTH_DATE")
    @Temporal(TemporalType.DATE)
    private Date birthDate;
    @Basic(optional = false)
    @Column(name = "HANDICAPPED")
    private Boolean handicapped;
    @JoinColumn(name = "PARENT", referencedColumnName = "ID")
    @ManyToOne
    private Employee parent;

    public Child() {
    }

    public Child(Integer id) {
        this.id = id;
    }

    public Child(String name, Date birthDate, Boolean handicapped, Employee parent) {
        this.name=name;
        this.birthDate = birthDate;
        this.handicapped = handicapped;
        this.parent=parent;
    }

    public Child(Integer id, String name, Date birthDate, Boolean handicapped, Employee parent) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.handicapped = handicapped;
        this.parent = parent;
    }

    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Boolean isHandicapped() {
        return handicapped;
    }

    public void setHandicapped(Boolean handicapped) {
        this.handicapped = handicapped;
    }

    public Employee getParent() {
        return parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParent(Employee parent) {
        this.parent = parent;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Child)) {
            return false;
        }
        Child other = (Child) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "hu.bh.entity.Child[ id=" + id + " ]";
    }

}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Balyi Zoltan
 */
@Entity
@Table(name = "EMPLOYEE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Employee.findAll", query = "SELECT e FROM Employee e")
    , @NamedQuery(name = "Employee.findById", query = "SELECT e FROM Employee e WHERE e.id = :id")
    , @NamedQuery(name = "Employee.findByFirstname", query = "SELECT e FROM Employee e WHERE e.firstname = :firstname")
    , @NamedQuery(name = "Employee.findByLastname", query = "SELECT e FROM Employee e WHERE e.lastname = :lastname")
    , @NamedQuery(name = "Employee.findByBirthdate", query = "SELECT e FROM Employee e WHERE e.birthdate = :birthdate")
    , @NamedQuery(name = "Employee.findByHandicapped", query = "SELECT e FROM Employee e WHERE e.handicapped = :handicapped")
    , @NamedQuery(name = "Employee.findByContractStyle", query = "SELECT e FROM Employee e WHERE e.contractStyle = :contractStyle")
    , @NamedQuery(name = "Employee.findByStartOfWork", query = "SELECT e FROM Employee e WHERE e.startOfWork = :startOfWork")
    , @NamedQuery(name = "Employee.findByEndOfWork", query = "SELECT e FROM Employee e WHERE e.endOfWork = :endOfWork")
    , @NamedQuery(name = "Employee.findByRamainingLastYearVacations", query = "SELECT e FROM Employee e WHERE e.ramainingLastYearVacations = :ramainingLastYearVacations")
    , @NamedQuery(name = "Employee.findByEmail", query = "SELECT e FROM Employee e WHERE e.email = :email")
    , @NamedQuery(name = "Employee.findByPassword", query = "SELECT e FROM Employee e WHERE e.password = :password")})
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "FIRSTNAME")
    private String firstname;
    @Basic(optional = false)
    @Column(name = "LASTNAME")
    private String lastname;
    @Basic(optional = false)
    @Column(name = "BIRTHDATE")
    @Temporal(TemporalType.DATE)
    private Date birthdate;
    @Basic(optional = false)
    @Column(name = "HANDICAPPED")
    private boolean handicapped;
    @Basic(optional = false)
    @Column(name = "CONTRACT_STYLE")
    private boolean contractStyle;
    @Basic(optional = false)
    @Column(name = "START_OF_WORK")
    @Temporal(TemporalType.DATE)
    private Date startOfWork;
    @Column(name = "END_OF_WORK")
    @Temporal(TemporalType.DATE)
    private Date endOfWork;
    @Column(name = "RAMAINING_LAST_YEAR_VACATIONS")
    private Short ramainingLastYearVacations;
    @Basic(optional = false)
    @Column(name = "EMAIL")
    private String email;
    @Basic(optional = false)
    @Column(name = "PASSWORD")
    private String password;
    @JoinColumn(name = "ID_DEPARTMENT", referencedColumnName = "ID")
    @ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private Department department;
    @JoinColumn(name = "ID_POSITION", referencedColumnName = "ID")
    @ManyToOne
    private Position position;

    public Employee() {
    }

    public Employee(Integer id) {
        this.id = id;
    }

    public Employee(String firstname, String lastname, Date birthdate, boolean handicapped, boolean contractStyle, Date startOfWork, Date endOfWork, Short ramainingLastYearVacations, String email, String password, Department department, Position position) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthdate = birthdate;
        this.handicapped = handicapped;
        this.contractStyle = contractStyle;
        this.startOfWork = startOfWork;
        this.endOfWork = endOfWork;
        this.ramainingLastYearVacations = ramainingLastYearVacations;
        this.email = email;
        this.password = password;
        this.department = department;
        this.position = position;
    }

    public Employee(String firstname, String lastname, Date birthdate, boolean handicapped, boolean contractStyle, Date startOfWork, Date endOfWork, Short ramainingLastYearVacations, String email, String password) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthdate = birthdate;
        this.handicapped = handicapped;
        this.contractStyle = contractStyle;
        this.startOfWork = startOfWork;
        this.endOfWork = endOfWork;
        this.ramainingLastYearVacations = ramainingLastYearVacations;
        this.email = email;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public boolean isHandicapped() {
        return handicapped;
    }

    public void setHandicapped(boolean handicapped) {
        this.handicapped = handicapped;
    }

    public boolean isContractStyle() {
        return contractStyle;
    }

    public void setContractStyle(boolean contractStyle) {
        this.contractStyle = contractStyle;
    }

    public Date getStartOfWork() {
        return startOfWork;
    }

    public void setStartOfWork(Date startOfWork) {
        this.startOfWork = startOfWork;
    }

    public Date getEndOfWork() {
        return endOfWork;
    }

    public void setEndOfWork(Date endOfWork) {
        this.endOfWork = endOfWork;
    }

    public Short getRamainingLastYearVacations() {
        return ramainingLastYearVacations;
    }

    public void setRamainingLastYearVacations(Short ramainingLastYearVacations) {
        this.ramainingLastYearVacations = ramainingLastYearVacations;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Employee)) {
            return false;
        }
        Employee other = (Employee) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "hu.bh.entity.Employee[ id=" + id + " ]";
    }

}

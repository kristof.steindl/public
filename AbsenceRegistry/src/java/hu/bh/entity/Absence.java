/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.entity;

import hu.bh.enums.AbsenceStatus;
import hu.bh.enums.AbsenceType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Balyi Zoltan
 */
@Entity
@Table(name = "ABSENCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Absence.findAll", query = "SELECT a FROM Absence a ORDER BY a.employee ASC, a.startDay DESC")
    , @NamedQuery(name = "Absence.findById", query = "SELECT a FROM Absence a WHERE a.id = :id")
    , @NamedQuery(name = "Absence.findByStartDay", query = "SELECT a FROM Absence a WHERE a.startDay = :startDay")
    , @NamedQuery(name = "Absence.findByEndDay", query = "SELECT a FROM Absence a WHERE a.endDay = :endDay")
    , @NamedQuery(name = "Absence.findByStatus", query = "SELECT a FROM Absence a WHERE a.status = :status")
    , @NamedQuery(name = "Absence.findByType", query = "SELECT a FROM Absence a WHERE a.type = :type")})
public class Absence implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
	
    @Basic(optional = false)
    @Column(name = "START_DAY")
    @Temporal(TemporalType.DATE)
    private Date startDay;
	
    @Basic(optional = false)
    @Column(name = "END_DAY")
    @Temporal(TemporalType.DATE)
    private Date endDay;
	
    @Basic(optional = false)
    @Column(name = "STATUS")
    private Integer status;
	
    @Basic(optional = false)
    @Column(name = "TYPE")
    private Integer type;
	
    @Lob
    @Column(name = "ABSENCE_COMMENT")
    private Serializable absenceComment;
	
    @JoinColumn(name = "ID_EMPLOYEE", referencedColumnName = "ID")
    @ManyToOne
    private Employee employee;

    public Absence() {
    }

    public Absence(Integer id) {
        this.id = id;
    }

    public Absence(Integer id, Date startDay, Date endDay, Integer status, Integer type) {
        this.id = id;
        this.startDay = startDay;
        this.endDay = endDay;
        this.status = status;
        this.type = type;
    }

    public Absence(Date startDay, Date endDay, Integer status, Integer type, Employee employee) {
        this.startDay = startDay;
        this.endDay = endDay;
        this.status = status;
        this.type = type;
        this.employee = employee;
    }
    
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartDay() {
        return startDay;
    }

    public void setStartDay(Date startDay) {
        this.startDay = startDay;
    }

    public Date getEndDay() {
        return endDay;
    }

    public void setEndDay(Date endDay) {
        this.endDay = endDay;
    }

    public AbsenceStatus getStatus() {
        return AbsenceStatus.parse(this.status);
    }

    public void setStatus(AbsenceStatus status) {
        this.status = status.getValue();
    }

    public AbsenceType getType() {
        return AbsenceType.parse(this.type);
    }

    public void setType(AbsenceType type) {
        this.type = type.getValue();
    }

    public Serializable getAbsenceComment() {
        return absenceComment;
    }

    public void setAbsenceComment(Serializable absenceComment) {
        this.absenceComment = absenceComment;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Absence)) {
            return false;
        }
        Absence other = (Absence) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "hu.bh.entity.Absence[ id=" + id + " ]";
    }

}

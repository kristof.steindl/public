/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.dto;

import hu.bh.entity.Employee;
import hu.bh.handler.DateAndCalendarHandler;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Klara
 */
public class ChildDTO  implements Serializable {

    private static final long serialVersionUID = 1L;
   
    private Integer id;
    private String name;
    private Date birthDate;
    private boolean handicapped;
    private EmployeeDTO parent;

    public ChildDTO() {
    }

    public ChildDTO(Integer id) {
        this.id = id;
    }

    public ChildDTO(String name, Date birthDate, boolean handicapped, EmployeeDTO parent) {
        this.name = name;
        this.birthDate = birthDate;
        this.handicapped = handicapped;
        this.parent = parent;
    }

    public ChildDTO(Integer id, String name, Date birthDate, boolean handicapped, EmployeeDTO parent) {
        this.id = id;
        this.name= name;
        this.birthDate = birthDate;
        this.handicapped = handicapped;
        this.parent=parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public String getStringBirthDate() {
		return DateAndCalendarHandler.getStringDate(this.birthDate); 
	}
    
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public boolean isHandicapped() {
        return handicapped;
    }

    public void setHandicapped(boolean handicapped) {
        this.handicapped = handicapped;
    }

    public EmployeeDTO getParent() {
        return parent;
    }

    public void setParent(EmployeeDTO parent) {
        this.parent = parent;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ChildDTO other = (ChildDTO) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
   

    @Override
    public String toString() {
        return "hu.bh.entity.Child[ id=" + id + " ]";
    }

}



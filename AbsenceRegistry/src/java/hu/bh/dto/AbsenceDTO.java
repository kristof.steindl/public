
package hu.bh.dto;

import hu.bh.enums.AbsenceStatus;
import hu.bh.enums.AbsenceType;
import hu.bh.handler.DateAndCalendarHandler;
import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;

public class AbsenceDTO implements Serializable {

    private Integer id;
    private Date startDay;
    private Date endDay;
    private Integer status = AbsenceStatus.PENDING.getValue();
    private Integer type;
    private Serializable absenceComment;
    private EmployeeDTO employee;
	private DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);

    public AbsenceDTO() {
    }

    public AbsenceDTO(Integer id) {
        this.id = id;
    }

	public AbsenceDTO(Integer id, Date startDay, Date endDay, Integer status, Integer type, Serializable absenceComment, EmployeeDTO employee) {
		this.id = id;
		this.startDay = startDay;
		this.endDay = endDay;
		this.status = status;
		this.type = type;
		this.absenceComment = absenceComment;
		this.employee = employee;
	}	
	
	public AbsenceDTO(Date startDay, Date endDay, Integer type, Serializable absenceComment, EmployeeDTO employee) {
        this.startDay = startDay;
        this.endDay = endDay;
        this.type = type;
		this.absenceComment = absenceComment;
		this.employee = employee;	
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStringStartDate() {
		return DateAndCalendarHandler.getStringDate(this.startDay); 
	}
	
	public Date getStartDay() {
		return startDay;
	}

	public String getDateFormattedStartDay() {
		return dateFormat.format(startDay);
	}
	
	public void setStartDay(Date startDay) {
		this.startDay = startDay;
	}
	
	public String getStringEndDate() {
		return DateAndCalendarHandler.getStringDate(this.endDay); 
	}

	public Date getEndDay() {
		return endDay;
	}
	
	public String getDateFormattedEndDay() {
		return dateFormat.format(endDay);
	}

	public void setEndDay(Date endDay) {
		this.endDay = endDay;
	}

    public AbsenceStatus getStatus() {
        return AbsenceStatus.parse(this.status);
    }

    public void setStatus(AbsenceStatus status) {
        this.status = status.getValue();
    }

    public AbsenceType getType() {
        return AbsenceType.parse(this.type);
    }

    public void setType(AbsenceType type) {
        this.type = type.getValue();
    }

	public Serializable getAbsenceComment() {
		return absenceComment;
	}

	public void setAbsenceComment(Serializable absenceComment) {
		this.absenceComment = absenceComment;
	}

	public EmployeeDTO getEmployee() {
		return employee;
	}

	public void setEmployee(EmployeeDTO employee) {
		this.employee = employee;
	}

    @Override
    public String toString() {
        return "hu.bh.entity.AbsenceDTO[ id=" + id + " ]";
    }

}

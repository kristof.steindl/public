/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author Balyi Zoltan
 */
public class PositionDTO implements Serializable {

    private Integer id;

    private String positionName;

    private boolean dangerous;

    public PositionDTO() {
    }

    public PositionDTO(Integer id) {
        this.id = id;
    }

    public PositionDTO(Integer id, String positionName, boolean dangerous) {
        this.id = id;
        this.positionName = positionName;
        this.dangerous = dangerous;
    }

    public PositionDTO(String positionName, boolean dangerous) {
        this.positionName = positionName;
        this.dangerous = dangerous;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public boolean isDangerous() {
        return dangerous;
    }

    public void setDangerous(boolean dangerous) {
        this.dangerous = dangerous;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PositionDTO)) {
            return false;
        }
        PositionDTO other = (PositionDTO) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Position{" + "id=" + id + ", positionName=" + positionName + ", dangerous=" + dangerous + '}';
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author Balyi Zoltan
 */
public class DepartmentDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String departmentName;

    private Integer departmentBossId;

    public DepartmentDTO() {
    }

    public DepartmentDTO(Integer id) {
        this.id = id;
    }

    public DepartmentDTO(String departmentName, Integer departmentBossId) {
        this.departmentName = departmentName;
        this.departmentBossId = departmentBossId;
    }

    public DepartmentDTO(String departmentName) {
        this.departmentName = departmentName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

	public Integer getDepartmentBossId() {
		return departmentBossId;
	}

	public void setDepartmentBossId(Integer departmentBossId) {
		this.departmentBossId = departmentBossId;
	}

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DepartmentDTO)) {
            return false;
        }
        DepartmentDTO other = (DepartmentDTO) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "hu.bh.entity.Department[ id=" + id + " name: " + departmentName + " ]";
    }

}

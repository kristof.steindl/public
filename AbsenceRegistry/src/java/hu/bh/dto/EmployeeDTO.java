/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Balyi Zoltan
 */
public class EmployeeDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation

    private Integer id;

    private String firstname;

    private String lastname;

    private Date birthdate;

    private boolean handicapped;

    private boolean contractStyle;

    private Date startOfWork;

    private Date endOfWork;

    private Short ramainingLastYearVacations;

    private String email;

    private String password;

    private DepartmentDTO department;

    private PositionDTO position;

    public EmployeeDTO() {
    }

    public EmployeeDTO(Integer id) {
        this.id = id;
    }

    public EmployeeDTO(String firstname, String lastname, Date birthdate, boolean handicapped, boolean contractStyle, Date startOfWork, Date endOfWork, Short ramainingLastYearVacations, String email, String password, DepartmentDTO department, PositionDTO position) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthdate = birthdate;
        this.handicapped = handicapped;
        this.contractStyle = contractStyle;
        this.startOfWork = startOfWork;
        this.endOfWork = endOfWork;
        this.ramainingLastYearVacations = ramainingLastYearVacations;
        this.email = email;
        this.password = password;
        this.department = department;
        this.position = position;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public boolean isHandicapped() {
        return handicapped;
    }

    public void setHandicapped(boolean handicapped) {
        this.handicapped = handicapped;
    }

    public boolean isContractStyle() {
        return contractStyle;
    }

    public void setContractStyle(boolean contractStyle) {
        this.contractStyle = contractStyle;
    }

    public Date getStartOfWork() {
        return startOfWork;
    }

    public void setStartOfWork(Date startOfWork) {
        this.startOfWork = startOfWork;
    }

    public Date getEndOfWork() {
        return endOfWork;
    }

    public void setEndOfWork(Date endOfWork) {
        this.endOfWork = endOfWork;
    }

    public Short getRamainingLastYearVacations() {
        return ramainingLastYearVacations;
    }

    public void setRamainingLastYearVacations(Short ramainingLastYearVacations) {
        this.ramainingLastYearVacations = ramainingLastYearVacations;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public DepartmentDTO getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentDTO department) {
        this.department = department;
    }

    public PositionDTO getPosition() {
        return position;
    }

    public void setPosition(PositionDTO position) {
        this.position = position;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmployeeDTO)) {
            return false;
        }
        EmployeeDTO other = (EmployeeDTO) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "hu.bh.entity.Employee[ id=" + id + " ]";
    }

    public String toStringToDropDown() {
        return firstname + " " + lastname + " (" + birthdate + ")";
    }
	
	    public String toStringAll() {
        return firstname + " " + lastname + " (" + birthdate + ")" + id + " "+ email + " "+ department + " "+ position + " ";
    }

}

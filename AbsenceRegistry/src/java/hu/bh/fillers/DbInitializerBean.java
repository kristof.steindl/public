
package hu.bh.fillers;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import hu.bh.dto.EmployeeDTO;
import hu.bh.ejb.EmployeeBean;
import hu.bh.entity.Absence;
import hu.bh.entity.Child;
import hu.bh.entity.Department;
import hu.bh.entity.Employee;
import hu.bh.entity.Position;
import hu.bh.fillers.TableFillerApp;
import hu.bh.handler.AbsenceCounterBean;
import hu.bh.handler.DateAndCalendarHandler;
import hu.bh.handler.WorkDayCounterBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
	


@Stateless
public class DbInitializerBean {
	
	@PersistenceContext(unitName = "AbsenceRegistryPU")
	private EntityManager em;

	public void initDb() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Department hr = new Department("HR");
        em.persist(hr);
        Department management = new Department("Management");
        em.persist(management);
        Department finance = new Department("Finance");
        em.persist(finance);
        Department it = new Department("IT");
        em.persist(it);

        Position employee = new Position("Employee", false);
        em.persist(employee);
        Position leader = new Position("Leader", true);
        em.persist(leader);
        Position admin = new Position("Admin", false);
        em.persist(admin);
        Position receptionist = new Position("Receptionist", false);
        em.persist(receptionist);

        try {
            Employee zoli = new Employee("Zoltán", "Bályi", sdf.parse("1980-10-10"), false, true, sdf.parse("2010-10-10"), null, (short) 12, "zoli@brandnewexceptions.com", "zolipass", management, leader);
            em.persist(zoli);
            Employee kornel = new Employee("Kornél", "Kenyeres", sdf.parse("1985-10-10"), false, true, sdf.parse("2010-10-10"), sdf.parse("2018-10-05"), (short) 10, "kornel@brandnewexceptions.com", "kornelpass", finance, receptionist);
            em.persist(kornel);
            Employee kristof = new Employee("Kristóf", "Steindl", sdf.parse("1990-10-10"), false, true, sdf.parse("2010-10-10"), null, (short) 11, "kristof@brandnewexceptions.com", "kristofpass", it, admin);
            em.persist(kristof);
            Employee klara = new Employee("Klára", "Theobald", sdf.parse("1995-10-10"), true, false, sdf.parse("2010-10-10"), null, (short) 13, "klara@brandnewexceptions.com", "klarapass", hr, employee);
            em.persist(klara);

            Child zolika = new Child("Zolika", sdf.parse("2000-10-10"), false, zoli);
            em.persist(zolika);
            Child kristofka = new Child("Kristófka", sdf.parse("2017-10-10"), false, kristof);
            em.persist(kristofka);
            Child klarika = new Child("Klárika", sdf.parse("2015-10-10"), true, klara);
            em.persist(klarika);
            Child petike = new Child("Petike", sdf.parse("2016-10-10"), true, klara);
            em.persist(petike);

            Absence azoli = new Absence(sdf.parse("2018-09-10"), sdf.parse("2018-10-11"), 2, 1, zoli);
            Absence azoli2 = new Absence(sdf.parse("2018-10-12"), sdf.parse("2018-11-11"), 2, 1, zoli);
            Absence akornel1 = new Absence(sdf.parse("2018-10-19"), sdf.parse("2018-11-11"), 2, 1, kornel);
            Absence akristof1 = new Absence(sdf.parse("2018-02-01"), sdf.parse("2018-02-06"), 2, 1, kristof);
			Absence akristof2 = new Absence(sdf.parse("2018-04-01"), sdf.parse("2018-04-06"), 2, 1, kristof);
			Absence akristof3 = new Absence(sdf.parse("2018-05-01"), sdf.parse("2018-05-10"), 3, 1, kristof);
			Absence akristof4 = new Absence(sdf.parse("2018-06-01"), sdf.parse("2018-06-10"), 2, 2, kristof);
			Absence akristof5 = new Absence(sdf.parse("2018-12-01"), sdf.parse("2018-12-03"), 2, 2, kristof);
            Absence aklara1 = new Absence(sdf.parse("2018-10-30"), sdf.parse("2018-11-11"), 2, 1, klara);

			em.persist(azoli);
			em.persist(azoli2);
			em.persist(akornel1);
			em.persist(akristof1);
			em.persist(akristof2);
			em.persist(akristof3);
			em.persist(akristof4);
			em.persist(akristof5);
			em.persist(aklara1);
			
            hr.setDepartmentBoss(klara);
            em.persist(hr);
            management.setDepartmentBoss(zoli);
            em.persist(management);
            finance.setDepartmentBoss(kornel);
            em.persist(finance);
            it.setDepartmentBoss(kristof);
            em.persist(it);

        } catch (ParseException ex) {
            Logger.getLogger(TableFillerApp.class.getName()).log(Level.SEVERE, null, ex);
        }
	}
	
}

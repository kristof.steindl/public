/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.fillers;

import hu.bh.entity.Absence;
import hu.bh.entity.Child;
import hu.bh.entity.Department;
import hu.bh.entity.Employee;
import hu.bh.entity.Position;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Klara
 */
public class TableFillerApp {

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("AbsenceRegistryPU");
    private static EntityManager em = emf.createEntityManager();

    public static void main(String[] args) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Department hr = new Department("HR");
        em.persist(hr);
        Department management = new Department("Management");
        em.persist(management);
        Department finance = new Department("Finance");
        em.persist(finance);
        Department it = new Department("IT");
        em.persist(it);

        Position employee = new Position("Employee", false);
        em.persist(employee);
        Position leader = new Position("Leader", true);
        em.persist(leader);
        Position admin = new Position("Admin", false);
        em.persist(admin);
        Position receptionist = new Position("Receptionist", false);
        em.persist(receptionist);

        try {
            Employee zoli = new Employee("Zoltán", "Bályi", sdf.parse("1980-10-10"), false, true, sdf.parse("2010-10-10"), sdf.parse("2018-10-05"), (short) 12, "zoli@brandnewexceptions.com", "zolipass", management, leader);
            em.persist(zoli);
            Employee kornel = new Employee("Kornél", "Kenyeres", sdf.parse("1985-10-10"), false, true, sdf.parse("2010-10-10"), null, (short) 10, "kornel@brandnewexceptions.com", "kornelpass", finance, receptionist);
            em.persist(kornel);
            Employee kristof = new Employee("Kristóf", "Steindl", sdf.parse("1990-10-10"), false, true, sdf.parse("2010-10-10"), sdf.parse("2018-10-05"), (short) 11, "kristof@brandnewexceptions.com", "kristofpass", it, admin);
            em.persist(kristof);
            Employee klara = new Employee("Klára", "Theobald", sdf.parse("1995-10-10"), true, false, sdf.parse("2010-10-10"), sdf.parse("2018-10-05"), (short) 13, "klara@brandnewexceptions.com", "klarapass", hr, employee);
            em.persist(klara);

            Child zolika = new Child("Zolika", sdf.parse("2000-10-10"), false, zoli);
            em.persist(zolika);
            Child kristofka = new Child("Kristófka", sdf.parse("2017-10-10"), false, kristof);
            em.persist(kristofka);
            Child klarika = new Child("Klárika", sdf.parse("2015-10-10"), true, klara);
            em.persist(klarika);
            Child petike = new Child("Petike", sdf.parse("2016-10-10"), true, klara);

            Absence azoli = new Absence(sdf.parse("2018-09-10"), sdf.parse("2018-10-11"), 1, 1, zoli);
            Absence azoli2 = new Absence(sdf.parse("2018-10-12"), sdf.parse("2018-11-11"), 1, 1, zoli);
            Absence akornel1 = new Absence(sdf.parse("2018-10-19"), sdf.parse("2018-11-11"), 1, 1, kornel);
            Absence akristof1 = new Absence(sdf.parse("2018-10-26"), sdf.parse("2018-11-11"), 1, 1, kristof);
            Absence aklara1 = new Absence(sdf.parse("2018-10-30"), sdf.parse("2018-11-11"), 1, 1, klara);

            hr.setDepartmentBoss(klara);
            em.persist(hr);
            management.setDepartmentBoss(zoli);
            em.persist(management);
            finance.setDepartmentBoss(kornel);
            em.persist(finance);
            it.setDepartmentBoss(kristof);
            em.persist(it);

        } catch (ParseException ex) {
            Logger.getLogger(TableFillerApp.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}

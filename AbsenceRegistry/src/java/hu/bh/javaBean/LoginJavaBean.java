
package hu.bh.javaBean;

public class LoginJavaBean {
	
	private int employeeId;
	private int positionId;
	private int departmentId;
	private boolean failedLogin;

	public LoginJavaBean() {
	}

	public LoginJavaBean(int employeeId, int positionId, int departmentId) {
		this.employeeId = employeeId;
		this.positionId = positionId;
		this.departmentId = departmentId;
	}

	@Override
	public String toString() {
		return "loginJavaBean{" + "employeeId=" + employeeId + ", positionId=" + positionId + ", departmentId=" + departmentId + '}';
	}

	public boolean isFailedLogin() {
		return failedLogin;
	}

	public void setFailedLogin(boolean failedLogin) {
		this.failedLogin = failedLogin;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public int getPositionId() {
		return positionId;
	}

	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	
}

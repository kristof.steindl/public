package hu.bh.dao;

import java.util.Date;

public class ChildDAO {

    private Long id;
    private Date birthDate;
    private boolean handicapped;
    private Long idParent;

    public ChildDAO() {
    }

    public ChildDAO(Long id, Date birthDate, boolean handicapped, Long idParent) {
        this.id = id;
        this.birthDate = birthDate;
        this.handicapped = handicapped;
        this.idParent = idParent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public boolean isHandicapped() {
        return handicapped;
    }

    public void setHandicapped(boolean handicapped) {
        this.handicapped = handicapped;
    }

    public Long getIdParent() {
        return idParent;
    }

    public void setIdParent(Long idParent) {
        this.idParent = idParent;
    }

}

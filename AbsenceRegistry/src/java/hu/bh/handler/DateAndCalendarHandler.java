
package hu.bh.handler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateAndCalendarHandler {

	public static Date getParsedDateFromRequest(String requestParameter) throws ParseException {
		SimpleDateFormat sdf = getSimpleDateFormat();
		Date date = sdf.parse(requestParameter);
		return date;
	}
	
	public static SimpleDateFormat getSimpleDateFormat() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf;
	}
	
	public static int getDayOfYear(Date date) {
		Calendar calendar = getCalendarFromDate(date);
		int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR); 
		return dayOfYear;
	}
	
	public static Calendar getCalendarFromDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar;
	}
	
	public static String getStringDate(Date date) {
		String stringDate;
		stringDate = getSimpleDateFormat().format(date);
		return stringDate;
	}

	
}


package hu.bh.handler;

import java.util.HashMap;
import java.util.Map;

public enum WorkDayType {
	    WORKDAY(0),
        EXTRAWORKDAY(1),
        RESTDAY(2),
        HOLIDAY(3);

        private final int index;
		private static Map workDayEnumMap = new HashMap<>();

        WorkDayType(final int newValue) {
            index = newValue;
        }
		
		static {
			for (WorkDayType workDayType : WorkDayType.values()) {
				workDayEnumMap.put(workDayType.index, workDayType);
			}
		}

    public static WorkDayType getWorkDayType(int index) {
        return (WorkDayType) workDayEnumMap.get(index);
    }
        public int getIndex() {
			return index; 
		}
}


package hu.bh.handler;

import hu.bh.exception.InvalidAbsenceRequest;
import hu.bh.mapper.*;
import hu.bh.dto.AbsenceDTO;
import hu.bh.dto.ChildDTO;
import hu.bh.dto.EmployeeDTO;
import hu.bh.ejb.AbsenceBean;
import hu.bh.ejb.ChildBean;
import hu.bh.ejb.EmployeeBean;
import hu.bh.enums.AbsenceStatus;
import hu.bh.enums.AbsenceType;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.PostActivate;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateful
@LocalBean
public class AbsenceCounterBean {
	
	@EJB
	AbsenceBean absenceBean;
	
	@EJB
	WorkDayCounterBean workDayCounterBean;
	
	@EJB
	ChildBean childBean;
	
	private ResourceBundle absConstResourceBundle;
	private Map<Integer, Integer> extraDaysForYearsMap;
	private Map<Integer, Integer> extraDaysForChildrenMap;	
	private EmployeeDTO employee;


	
	@PostConstruct
	public void initConstants() {
		
		absConstResourceBundle = ResourceBundle.getBundle("absenceContstants");
		
		ResourceBundle extraDaysForAgeResourceBundle = ResourceBundle.getBundle("extraDaysByAge");
		extraDaysForYearsMap = AbsenceInitializer.initExtraDaysForYearsMap(extraDaysForAgeResourceBundle, absConstResourceBundle);
		
		ResourceBundle extraDaysForChildrenResourceBundle = ResourceBundle.getBundle("extraDaysForChildren");
		extraDaysForChildrenMap = AbsenceInitializer.getMapFromResourceBundle(extraDaysForChildrenResourceBundle);	
	}
	
	public void validateAbsenceRequest(AbsenceDTO requiredAbsence) throws InvalidAbsenceRequest {
		if (requiredAbsence.getType() == AbsenceType.GENERAL) {
			employee = requiredAbsence.getEmployee();
			int requiredGeneralDays = workDayCounterBean.countWorkingDays(requiredAbsence);
			int availableDaysOff = getAvailableDaysOffAndCheckOverlapping(requiredAbsence);
			if (requiredGeneralDays > availableDaysOff) {
				throw new InvalidAbsenceRequest(
						"Nem valid szabadságkérelem! "
						+ "(igényelt: " + requiredGeneralDays 
						+ ", igénybevehető: " + availableDaysOff + ")");
			}				
		}
	}
	

	private int getAvailableDaysOffAndCheckOverlapping(AbsenceDTO requiredAbsence) throws InvalidAbsenceRequest {
		int annualVacations = getAnnualVacations();
		int takenVacations = getTakenVacationsAndCheckOverlapping(requiredAbsence);
		return (annualVacations - takenVacations);
	}
	
	private int getTakenVacationsAndCheckOverlapping(AbsenceDTO requiredAbsence) throws InvalidAbsenceRequest {
		List<AbsenceDTO> absenceList = absenceBean.getAbsenceDtoListByEmployeeDto(employee);
		int takenVacations = 0;
		for (AbsenceDTO absence : absenceList) {
			checkOverLapping(absence, requiredAbsence);
			takenVacations += getTakenVacation(absence);
		}
		return takenVacations;
	}
	
	private int  getTakenVacation(AbsenceDTO absence) {			
		int takenVacations = 0;
		if (((absence.getStatus() ==  AbsenceStatus.APPROVED) || (absence.getStatus() ==  AbsenceStatus.PENDING))
				&& absence.getType()== AbsenceType.GENERAL) {
			takenVacations = workDayCounterBean.countWorkingDays(absence);
		}
		return takenVacations;
	}
		
	private int getAnnualVacations() {
		int annualVacationByYear = 0;
		annualVacationByYear = 
				Integer.parseInt(absConstResourceBundle.getString("BASENUMBEROFVACATION"))
				+ extraDaysForYearsMap.get(getAge(employee.getBirthdate()))
				+ extraDaysForChildren()
				+ extraDaysForEmployeeHandicapped();					
		return annualVacationByYear;
	}
	
	private int getAge(Date birthDate) {
		LocalDate birtDateLocal = convertToLocalDateViaInstant(birthDate);
		LocalDate nowLocal = convertToLocalDateViaInstant(new Date());
		Period age = Period.between(birtDateLocal, nowLocal);
		return age.getYears();
	}
	
	private int extraDaysForChildren() { //Refactorálni
		int extraDaysForChildren = 0;
		int numberOfChildrenUnder16 = 0;
		List<ChildDTO> childList = childBean.getChildDtoListByEmployeeDto(employee);
		for (ChildDTO child : childList) {
			int childAge = getAge(child.getBirthDate());
			if (childAge < 16) {
				if (child.isHandicapped() == true) {
					extraDaysForChildren += Integer.parseInt(absConstResourceBundle.getString("HANDICAPPEDCHILD"));
				}
				numberOfChildrenUnder16 ++;
			}
		}
		extraDaysForChildren += extraDaysForChildrenMap.get(numberOfChildrenUnder16);
		return extraDaysForChildren;
	}
			
	private LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
		return dateToConvert.toInstant()
		  .atZone(ZoneId.systemDefault())
		  .toLocalDate();
	}
	
	private int extraDaysForEmployeeHandicapped() {
		int extraDaysForEmployeeHandicapped = 0;
		if (employee.isHandicapped()) {
			extraDaysForEmployeeHandicapped += Integer.parseInt(absConstResourceBundle.getString("EMPLOYEEHANDICAPPED"));
		}
		return extraDaysForEmployeeHandicapped;			
	}

	public void checkOverLapping(AbsenceDTO absence, AbsenceDTO requiredAbsence) throws InvalidAbsenceRequest {
		if (absence.getStartDay().before(requiredAbsence.getEndDay()) && requiredAbsence.getStartDay().before(absence.getEndDay())) {
			throw new InvalidAbsenceRequest("A szabadságkérelem átlapol egy regisztrált szabadsággal/szabadságkérelemmel!");
		}
		if (absence.getEndDay().after(requiredAbsence.getStartDay()) && requiredAbsence.getStartDay().after(absence.getEndDay())) {
			throw new InvalidAbsenceRequest("A szabadságkérelem átlapol egy regisztrált szabadsággal/szabadságkérelemmel!");
		}
	}



}

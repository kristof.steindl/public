
package hu.bh.handler;

import hu.bh.dto.AbsenceDTO;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;


@Stateless
public class WorkDayCounterBean {

	private Map<Date, Integer> workDayMap;
	
	@PostConstruct
	public void initWorkDayMap() {
		ResourceBundle workDayResourceBundle = ResourceBundle.getBundle("workDayCalendar");
		workDayMap = AbsenceInitializer.getWorkDayMapFromProperties(workDayResourceBundle);
	}
	
	public WorkDayType getWorkDayType(Date date) {
		WorkDayType workDayType;
		Integer workDayTypeIndex = workDayMap.get(date);
		if (null == workDayTypeIndex) {
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
			if (dayOfWeek == 1) {
				workDayType = WorkDayType.HOLIDAY;
			} 
			else if (dayOfWeek == 7) {
				workDayType = WorkDayType.RESTDAY;
			}
			else {
				workDayType = WorkDayType.WORKDAY;
			}		
		} else {
			workDayType = WorkDayType.getWorkDayType(workDayTypeIndex);
		}
		return workDayType;
	}

	public int countWorkingDays(AbsenceDTO adto) {
		int requiredWorkingDays = 0;
		Calendar start = Calendar.getInstance();
		start.setTime(adto.getStartDay());
		Calendar end = Calendar.getInstance();
		end.setTime(adto.getEndDay());
		end.add(Calendar.DATE, 1);
		for (Date date = start.getTime(); start.before(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
			WorkDayType dayType = getWorkDayType(date);
			if (
					dayType == WorkDayType.WORKDAY ||
					dayType == WorkDayType.EXTRAWORKDAY
					) {
				requiredWorkingDays++;
			}
		}
		return requiredWorkingDays;
	}
}

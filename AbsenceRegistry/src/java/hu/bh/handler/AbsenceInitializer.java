
package hu.bh.handler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;

public class AbsenceInitializer {

	public static ResourceBundle initResourceBundle(String propertiesName) {
		ResourceBundle resourceBundle = ResourceBundle.getBundle(propertiesName);
		return resourceBundle;
	}
	
//	public static Properties initProperties(String path) {
//		Properties properties = new Properties();
//		try (InputStream input = new FileInputStream(new File(path))){       
//			properties.load(input);
//		} catch (IOException ex) {
//			ex.printStackTrace();
//		}
//		return properties;
//	}
	
	public static Map<Integer, Integer> initExtraDaysForYearsMap(ResourceBundle extraDaysForAgeResourceBundle, ResourceBundle absConstResourceBundle) {
		int maxAge = 120;
		int plusDays = 0;
		int underAgeLimit = Integer.parseInt(absConstResourceBundle.getString("UNDERAGELIMIT"));
		int underAgePlusDays = Integer.parseInt(absConstResourceBundle.getString("UNDERAGEPLUSDAYS"));
		Map<Integer, Integer> extraDaysForYearsMap = getMapFromResourceBundle(extraDaysForAgeResourceBundle);
		int year;
		for (year = 0; year < underAgeLimit; year++) {
			extraDaysForYearsMap.put(year, underAgePlusDays);			
		}
		for (year = underAgeLimit; year < maxAge; year++) {
			if (extraDaysForYearsMap.get(year) != null) {
				plusDays = extraDaysForYearsMap.get(year);
			}
			extraDaysForYearsMap.put(year, plusDays);
		}
        return extraDaysForYearsMap;
	}

	public static Map<Integer, Integer> getMapFromResourceBundle(ResourceBundle resourceBundle) {
		Map<Integer, Integer> map = new HashMap<>();
		Enumeration<String> keys = resourceBundle.getKeys();
		while (keys.hasMoreElements()) {
			String key = (keys.nextElement());
			map.put(Integer.parseInt(key), Integer.parseInt(resourceBundle.getString(key)));
		}
		return map;
	}
	
	public static Map<Date, Integer> getWorkDayMapFromProperties(ResourceBundle workDayResourceBundle) {
		Map<Date, Integer> workDayMap = new HashMap<>();
		Enumeration<String> keys = workDayResourceBundle.getKeys();
		while (keys.hasMoreElements()) {
			String formattedDate = keys.nextElement();
			SimpleDateFormat format = DateAndCalendarHandler.getSimpleDateFormat();
			try {
				Date myDate = format.parse(formattedDate);
				workDayMap.put(myDate, Integer.parseInt(workDayResourceBundle.getString(formattedDate)));
			} catch (ParseException ex) {
				Logger.getLogger(AbsenceInitializer.class.getName()).log(Level.SEVERE, null, ex);
			}			
		}
		return workDayMap;
	}

}

<%--
    Document   : child_form
    Created on : 2018.09.15., 22:26:22
    Author     : root
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"></jsp:include>
<jsp:useBean id="childDTO" scope="session" class="hu.bh.dto.ChildDTO"></jsp:useBean>
    <div class="container">
        <div class="jumbotron">
            <h1>Child form</h1>
            <form method="POST">
                <div class="form-group">
                    <label for="child_name">Név</label>
                    <input type="text" class="form-control" id="child_name" name="child_name" value="<c:if test="${childDTO != null && childDTO.name != null}"><jsp:getProperty name="childDTO" property="name"/></c:if>">
                </div>
                <div class ="form-group">
                    <label for="date_of_birth">Születési idő</label>
                    <input type="date" class="form-control" id="date_of_birth" name="date_of_birth" value="<c:if test="${childDTO != null && childDTO.birthDate != null}"><c:out value="${childDTO.getStringBirthDate()}"/></c:if>">
                </div>
                <div class="form-group">
                    <label for="handicapped">Fogyatékkal élő</label>
                    <select name="handicapped" id="handicapped" class="form-control">
                        <option value="1"<c:if test="${childDTO != null && childDTO.handicapped}"> selected="selected"</c:if>>Yes</option>
                    <option value="0"<c:if test="${childDTO != null && !childDTO.handicapped}"> selected="selected"</c:if>>No</option>
                    </select>
                </div>
                <div class ="form-group">
                    <label for="parent">Parent</label>
                    <select name = "parent">
                        <option value="0"></option>
                    <c:forEach items="${parent}" var="item">
                        <option <c:if test="${childDTO != null && (childDTO.getParent().getId()).equals(item.getId())}"><c:out value="selected=\"selected\""/></c:if> value="${item.getId()}"><c:out value="${item.toStringToDropDown()}" /></option>
                    </c:forEach>
                </select>
            </div>
            <button type="submit" class="btn btn-success">
                <i class="far fa-save"></i>
                Save
            </button>
        </form>
    </div>
</div>
<jsp:include page="footer.jsp"></jsp:include>


<%--</body>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Child form</title>
</head>
<body>
    <h1>Child form</h1>
    <form method="POST">
        Id: <input type="text" name="id" value="" disabled="" />
        <br>
        Name: <input type="text" name="Name" value="" />
        <br>
        Date of Birth: <input type="text" name="date_of_birth" value="" />
        <br>
        Parent:
        <select name = "parent">
        <c:out value="${parent}"/>
        <option value=""></option>
        <c:forEach items="${parent}" var="item">
            <option value="${item.getId()}"><c:out value="${item.getLastname()} "/><c:out value="${item.getFirstname()}"/></option>
        </c:forEach>
    </select>
    <br>
    Handicapped: <select name="handicapped">
        <option value="-1">Select</option>
        <option value="1">Yes</option>
        <option value="0">No</option>
    </select>
    <input type="submit" value="Save" />
</form>
</body>
</html>
--%>

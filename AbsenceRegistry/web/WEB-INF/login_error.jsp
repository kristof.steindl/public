
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Failed login</title>
    </head>
    <body>
        <h1>Failed login</h1>
        <h3>${message}</h3>
		<form method="GET" action="/AbsenceRegistry/LoginServlet">
            <button type="submit" class="btn btn-success" name="logaction" value="login">Újra</button><br>    
        </form>
    </body>
</html>

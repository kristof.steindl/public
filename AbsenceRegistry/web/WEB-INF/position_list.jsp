<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.List"%>
<%@page import="hu.bh.entity.Position"%>
<jsp:include page="header.jsp"></jsp:include>
    <div class="container">
    <% if (request.getAttribute("errorMessage") != null) {%>
    <div class="row">
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Hiba!</h4>
            <p><%=request.getAttribute("errorMessage")%></p>
        </div>
    </div>
    <% }%>
    <div class="row">
        <h4><%=request.getAttribute("title")%></h4>
    </div>
    <div class="row">

        <div class="menu">
            <a href="<%=request.getAttribute("contextPath")%>/PositionServlet" type="button" class="btn btn-success">
                <i class="fas fa-plus-circle"></i>
                Új munkakör felvétele
            </a>
        </div>
    </div>
    <div class="row">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Munkakör név</th>
                    <th scope="col">Veszélyes</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <c:choose>
                    <c:when test="${positionList.isEmpty()==false}">
                        <c:forEach items="${positionList}" var="position">
                            <tr>
                                <td>
                                    <c:out value="${position.getId()}" />
                                </td>
                                <td>
                                    <c:out value="${position.getPositionName()}" />
                                </td>
                                <td>
                                    <c:choose>
                                        <c:when test="${position.isDangerous()}">
                                            igen
                                        </c:when>
                                        <c:otherwise>
                                            nem
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td>
                                    <div class="btn-group float-right" role="group">
                                        <a href="#" type="button" class="btn btn-info" title="Nézet">
                                            <i class="fas fa-search-plus"></i>
                                        </a>
                                        <a href="<%=request.getAttribute("contextPath")%>/PositionServlet?id=<c:out value="${position.getId()}" />" type="button" class="btn btn-info" title="Szerkesztés">
                                            <i class="far fa-edit"></i>
                                        </a>
                                        <a href="#" type="button" class="btn btn-info" title="Törlés">
                                            <i class="far fa-times-circle"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <tr><td colspan="4">Még nincsenek munkakörök.</td></tr>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>
</div>
<jsp:include page="footer.jsp"></jsp:include>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"></jsp:include>
<jsp:useBean id="positionDTO" scope="request" class="hu.bh.dto.PositionDTO"></jsp:useBean>
    <div class="container">
    <% if (request.getAttribute("errorMessage") != null) {%>
    <div class="row">
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Hiba!</h4>
            <p><%=request.getAttribute("errorMessage")%></p>
        </div>
    </div>
    <% }%>
    <div class="jumbotron">
        <h1>Munkakör űrlap</h1>
        <form method="POST">
            <div class="form-group">
                <label for="position_name">Név</label>
                <input type="text" class="form-control" id="position_name" name="position_name" value="<c:if test="${positionDTO != null && positionDTO.positionName != null}"><jsp:getProperty name="positionDTO" property="positionName"/></c:if>">
                </div>
                <div class="form-group">
                    <label for="dangerous">Veszélyes</label>
                    <select name="dangerous" id="dangerous" class="form-control">
                        <option value="1"<c:if test="${positionDTO != null && positionDTO.dangerous}"> selected="selected"</c:if>>Igen</option>
                    <option value="0"<c:if test="${positionDTO != null && !positionDTO.dangerous}"> selected="selected"</c:if>>Nem</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-success">
                    <i class="far fa-save"></i>
                    Mentés
                </button>
            </form>
        </div>
    </div>
<jsp:include page="footer.jsp"></jsp:include>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.List"%>
<%@page import="hu.bh.entity.Absence"%>
<jsp:include page="header.jsp"></jsp:include>
    <div class="container">
    <% if (request.getAttribute("errorMessage") != null) {%>
    <div class="alert alert-danger" role="alert">
        <h4 class="alert-heading">Error!</h4>
        <p><%=request.getAttribute("errorMessage")%></p>
    </div>
    <% }%>
    <div class="row">
        <h4><%=request.getAttribute("title")%></h4>
    </div>
    <div class="row">
        <div class="menu">
            <a href="<%=request.getAttribute("contextPath")%>/AbsenceServlet" type="button" class="btn btn-success">
                <i class="fas fa-plus-circle"></i>
                Add new absence
            </a>
        </div>
    </div>
    <div class="row">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Employee name</th>
                    <th scope="col">Start date</th>
                    <th scope="col">End date</th>
                    <th scope="col">Type</th>
                    <th scope="col">Status</th>
                </tr>
            </thead>
            <tbody>
                <c:choose>
                    <c:when test="${absenceList.isEmpty()==false}">
                        <c:forEach items="${absenceList}" var="absence">
                            <tr>
                                <td>
                                    <c:out value="${absence.getId()}" />
                                </td>
                                <td>
                                    <c:out value="${absence.getEmployee().getFirstname()}" />

                                    <c:out value="${absence.getEmployee().getLastname()}" />
                                </td>
                                <td>
                                    <c:out value="${absence.getDateFormattedStartDay()}" />
                                </td>
                                <td>
                                    <c:out value="${absence.getDateFormattedEndDay()}" />
                                </td>
                                <td>
                                    <c:out value="${absence.getStatus().toHunString()}" />
                                </td>
                                <td>
                                    <c:out value="${absence.getType().toHunString()}" />
                                </td>
                                <td>
                                    <div class="btn-group float-right" role="group">


                                        <a href="<%=request.getAttribute("contextPath")%>/AbsenceServlet?id=<c:out value="${absence.getId()}" />" type="button" class="btn btn-info" title="Edit">
                                            <i class="far fa-edit"></i>
                                        </a>
                                        <form method="POST" action="<c:out value="${contextPath}"/>/AbsenceDeleteServlet">
                                            <button name="absenceDTOId" value="${absence.getId()}" type="submit" class="btn btn-info" title="delete" >
                                                <i class="far fa-times-circle"></i>
                                            </button>
                                        </form>

                                    </div>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <tr><td colspan="4">No absence yet.</td></tr>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>
</div>
<jsp:include page="footer.jsp"></jsp:include>
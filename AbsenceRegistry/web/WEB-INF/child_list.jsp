<%--
    Document   : child_list
    Created on : 2018.10.06., 15:16:07
    Author     : Klara
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.List"%>
<%@page import="hu.bh.entity.Child"%>
<jsp:include page="header.jsp"></jsp:include>
    <div class="container">
        <div class="row">
            <h4><%=request.getAttribute("title")%></h4>
    </div>
    <div class="row">

        <div class="menu">
            <a href="<%=request.getAttribute("contextPath")%>/ChildServlet" type="button" class="btn btn-success">
                <i class="fas fa-plus-circle"></i>
                Add new child
            </a>
            <div class="row">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Child name</th>
                            <th scope="col">Birth date
                            <th scope="col">Hendicapped</th>
                            <th scope="col">Parent</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:choose>
                            <c:when test="${children.isEmpty()==false}">
                                <c:forEach items="${children}" var="child">
                                    <tr>
                                        <td>
                                            <c:out value="${child.getId()}" />
                                        </td>
                                        <td>
                                            <c:out value="${child.getName()}" />
                                        </td>
                                        <td>
                                            <c:out value="${child.getBirthDate()}" />
                                        </td>
                                        <td>
                                            <c:out value="${child.isHandicapped()}" />
                                        </td>
                                        <td>
                                            <c:out value="${child.getParent().getFirstname()} ${child.getParent().getLastname()}" />
                                        </td>
                                        <td>
                                            <div class="btn-group float-right" role="group">
                                                <a href="#" type="button" class="btn btn-info" title="View">
                                                    <i class="fas fa-search-plus"></i>
                                                </a>
                                                <a href="<%=request.getAttribute("contextPath")%>/ChildServlet?id=<c:out value="${child.getId()}" />" type="button" class="btn btn-info" title="Edit">
                                                    <i class="far fa-edit"></i>
                                                </a>
                                                <form method="POST" action="<c:out value="${contextPath}"/>/ChildDeleteServlet">
                                                    <button type="submit" name="childDTOId" value="${child.getId()}" title="Delete" class="btn btn-info"><i class="far fa-times-circle"></i></button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <tr><td colspan="4">No child yet.</td></tr>
                            </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
            </div>
        </div>
        <jsp:include page="footer.jsp"></jsp:include>


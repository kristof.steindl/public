
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="header.jsp"></jsp:include>
    <h1>Távollét felvitele</h1>
    <form method="POST" action="<c:out value="${contextPath}"/>/AbsenceServlet">
    <input type="hidden" name="absenceId" value="${absenceDTO.getId()}">
    Távollét kezdete:
    <input type="date" class="form-control" name="startDate" value="<c:if test="${absenceDTO != null}"><c:out value="${absenceDTO.getStringStartDate()}"/></c:if>"/>
        <br>
        Távollét vége:
        <input type="date" class="form-control" name="endDate" value="<c:if test="${absenceDTO != null}"><c:out value="${absenceDTO.getStringEndDate()}"/></c:if>"/>
        <br>
        Dolgozó:
        <select name="employeeId"  class="form-control" >
        <c:if test="${absenceDTO != null}">
            <option value="${absenceDTO.getEmployee().getId()}"><c:out value="${absenceDTO.getEmployee().toStringToDropDown()}" /></option>
        </c:if>
        <c:if test="${absenceDTO == null}">
            <option></option>
            <c:forEach items="${employeeList}" var="item">
                <option <c:if test="${absenceDTO != null && (absenceDTO.getEmployee().getId()).equals(item.getId())}"><c:out value="selected=\"selected\""/></c:if> value="${item.getId()}"><c:out value="${item.toStringToDropDown()}" /></option>
            </c:forEach>
        </c:if>
    </select>

    <br>
    Távollét típusa:
    <select name="type" class="form-control" >
        <c:forEach items="${absenceTypeList}" var="item">
            <option <c:if test="${absenceDTO != null && (absenceDTO.getType()).equals(item)}"><c:out value="selected=\"selected\""/></c:if> value="${item.getValue()}"><c:out value="${item.toHunString()}" /></option>
        </c:forEach>
    </select><br>
    <button type="submit" value="Save" class="btn btn-success">Mentés</button>
    <jsp:include page="footer.jsp"></jsp:include>

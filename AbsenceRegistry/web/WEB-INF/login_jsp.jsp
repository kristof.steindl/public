
<%@page import="hu.bh.javaBean.LoginJavaBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:useBean id="person" class="hu.bh.javaBean.LoginJavaBean" scope="session" />
<jsp:setProperty name="person" property="*"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" 
            integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" 
            crossorigin="anonymous">
        
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" 
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" 
        crossorigin="anonymous"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" 
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" 
        crossorigin="anonymous"> </script>


        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" 
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" 
        crossorigin="anonymous"></script>
    </head>
    <body>
        <c:if test="${loginJavaBean.employeeId <= 0}">
            <h1>Login</h1>
            <form method="POST" action="/AbsenceRegistry/LoginServlet">
                e-mail: <input type="email" class="form-control" name="email" value=""/><br>
                jelszó: <input type="password" class="form-control" name="password" value=""/><br>
                <button type="submit" class="btn btn-success" name="logaction" value="login" >Bejelentkezés</button><br>
            </form>
        </c:if>
        <c:if test="${loginJavaBean.employeeId > 0}">
            <h1>Be vagy jelentkezve</h1>
            <form method="GET" action="/AbsenceRegistry/LoginServlet">
                <button type="submit" class="btn btn-danger" name="logaction" value="logout">Kijelentkezés</button><br>    
            </form>
        </c:if>
    </body>
</html>

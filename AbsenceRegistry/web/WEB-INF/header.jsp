<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><%=request.getAttribute("title")%></title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link rel="stylesheet" href="static/css/style.css">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    </head>
    <body>
        <nav class="navbar navbar-dark navbar-expand-lg navbar-light bg-dark">
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="<%=request.getAttribute("contextPath")%>/AbsenceListServlet">T�voll�t</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="<%=request.getAttribute("contextPath")%>/EmployeeListServlet">Dolgoz�</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="<%=request.getAttribute("contextPath")%>/ChildListServlet">Gyermek</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="<%=request.getAttribute("contextPath")%>/DepartmentListServlet">Szervezeti egys�g</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="<%=request.getAttribute("contextPath")%>/PositionListServlet">Munkak�r</a>
                    </li>
                </ul>
                <a class="btn btn-danger"  href="<%=request.getAttribute("contextPath")%>/LogoutServlet">Kijelentkez�s</a>
            </div>
        </nav>
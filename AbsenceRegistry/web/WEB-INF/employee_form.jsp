<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="header.jsp"></jsp:include>
<jsp:useBean id="employeeDTO" scope="session" class="hu.bh.dto.EmployeeDTO"></jsp:useBean>
    <div class="container">
    <% if (request.getAttribute("errorMessage") != null) {%>
    <div class="row">
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Hiba!</h4>
            <p><%=request.getAttribute("errorMessage")%></p>
        </div>
    </div>
    <% }%>
    <div class="jumbotron">
        <h1>Dolgozó űrlap</h1>
        <form method="POST">
            <div class="form-group">
                <label for="firstname">Keresztnév</label>
                <input type="text" class="form-control" id="firstname" name="firstname" value="<c:if test="${employeeDTO != null && employeeDTO.firstname != null}"><jsp:getProperty name="employeeDTO" property="firstname"/></c:if>">
                </div>
                <div class="form-group">
                    <label for="lastname">Vezetéknév</label>
                    <input type="text" class="form-control" id="lastname" name="lastname" value="<c:if test="${employeeDTO != null && employeeDTO.lastname != null}"><jsp:getProperty name="employeeDTO" property="lastname"/></c:if>">
                </div>
                <div class="form-group">
                    <label for="birthdate">Születési idő</label>
                    <input type="date" class="form-control" id="birthdate" name="birthdate" value="<c:if test="${employeeDTO != null && employeeDTO.birthdate != null}"><fmt:formatDate value="${employeeDTO.birthdate}" pattern="yyyy-MM-dd"/></c:if>">
                </div>
                <div class="form-group">
                    <label for="handicapped">Fogyatékkal élő</label>
                    <select name="handicapped" id="handicapped" class="form-control">
                        <option value="1"<c:if test="${employeeDTO != null && employeeDTO.handicapped}"> selected </c:if>>Igen</option>
                    <option value="0"<c:if test="${employeeDTO != null && !employeeDTO.handicapped}"> selected </c:if>>Nem</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="contractStyle">Szerződéses</label>
                    <select name="contractStyle" id="contractStyle" class="form-control">
                        <option value="1"<c:if test="${employeeDTO != null && employeeDTO.contractStyle}"> selected </c:if>>Igen</option>
                    <option value="0"<c:if test="${employeeDTO != null && !employeeDTO.contractStyle}"> selected </c:if>>Nem</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="startOfWork">Munkábaállás kezdete</label>
                    <input type="date" class="form-control" id="startOfWork" name="startOfWork" value="<c:if test="${employeeDTO != null && employeeDTO.startOfWork != null}"><fmt:formatDate value="${employeeDTO.startOfWork}" pattern="yyyy-MM-dd"/></c:if>">
                </div>
                <div class="form-group">
                    <label for="endOfWork">Munkábaállás vége</label>
                    <input type="date" class="form-control" id="endOfWork" name="endOfWork" value="<c:if test="${employeeDTO != null && employeeDTO.endOfWork != null}"><fmt:formatDate value="${employeeDTO.endOfWork}" pattern="yyyy-MM-dd"/></c:if>">
                </div>
                <div class="form-group">
                    <label for="ramainingLastYearVacations">Elmúlt évről áthozott szabadság napok száma</label>
                    <input type="number" min="0" max="99" class="form-control" id="ramainingLastYearVacations" name="ramainingLastYearVacations" value="<c:if test="${employeeDTO != null && employeeDTO.ramainingLastYearVacations != null}"><jsp:getProperty name="employeeDTO" property="ramainingLastYearVacations"/></c:if>">
                </div>
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" id="email" name="email" value="<c:if test="${employeeDTO != null && employeeDTO.email != null}"><jsp:getProperty name="employeeDTO" property="email"/></c:if>">
                </div>
                <div class="form-group">
                    <label for="password">Jelszó</label>
                    <input type="password" class="form-control" id="password" name="password" value="<c:if test="${employeeDTO != null && employeeDTO.password != null}"><jsp:getProperty name="employeeDTO" property="password"/></c:if>">
                </div>
                <div class="form-group">
                    <label for="password_again">Jelszó újra</label>
                    <input type="password" class="form-control" id="password_again" name="password_again" value="<c:if test="${employeeDTO != null && employeeDTO.password != null}"><jsp:getProperty name="employeeDTO" property="password"/></c:if>">
                </div>
                <div class="form-group">
                    <label for="department">Szervezeti egység</label>
                <c:choose>
                    <c:when test="${departments.isEmpty()==false}">
                        <select name="department" id="department" class="form-control">
                            <option value="">Kérem válasszon!</option>
                            <c:forEach items="${departments}" var="department">
                                <option value="<c:out value="${department.getId()}"/>" <c:if test="${employeeDTO != null && employeeDTO.department.id == department.getId()}"> selected </c:if>><c:out value="${department.getDepartmentName()}"/></option>
                            </c:forEach>
                        </select>
                    </c:when>
                    <c:otherwise>
                        Nincsenek felvéve szervezeti egységek.
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="form-group">
                <label for="position">Munkakör</label>
                <c:choose>
                    <c:when test="${positions.isEmpty()==false}">
                        <select name="position" id="position" class="form-control">
                            <option value="">Kérem válasszon!</option>
                            <c:forEach items="${positions}" var="position">
                                <option value="<c:out value="${position.getId()}"/>" <c:if test="${employeeDTO != null && employeeDTO.position.id == position.getId()}"> selected </c:if>><c:out value="${position.getPositionName()}"/></option>
                            </c:forEach>
                        </select>
                    </c:when>
                    <c:otherwise>
                        Nincsenek felvéve munkakörök.
                    </c:otherwise>
                </c:choose>
            </div>
            <button type="submit" class="btn btn-success">
                <i class="far fa-save"></i>
                Mentés
            </button>
        </form>
    </div>
</div>
<jsp:include page="footer.jsp"></jsp:include>


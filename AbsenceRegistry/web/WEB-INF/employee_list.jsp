<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.List"%>
<%@page import="hu.bh.entity.Position"%>
<jsp:include page="header.jsp"></jsp:include>
    <div class="container">
    <% if (request.getAttribute("errorMessage") != null) {%>
    <div class="row">
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Hiba!</h4>
            <p><%=request.getAttribute("errorMessage")%></p>
        </div>
    </div>
    <% }%>
    <div class="row">
        <h4><%=request.getAttribute("title")%></h4>
    </div>
    <div class="row">

        <div class="menu">
            <a href="<%=request.getAttribute("contextPath")%>/EmployeeServlet" type="button" class="btn btn-success">
                <i class="fas fa-plus-circle"></i>
                Új dolgozó felvitele
            </a>
        </div>
    </div>
    <div class="row">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Keresztnév</th>
                    <th scope="col">Vezetéknév</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Szervezeti egység</th>
                    <th scope="col">Munkakör</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <c:choose>
                    <c:when test="${employeeList.isEmpty()==false}">
                        <c:forEach items="${employeeList}" var="employee">
                            <tr>
                                <td>
                                    <c:out value="${employee.getId()}" />
                                </td>
                                <td>
                                    <c:out value="${employee.getFirstname()}" />
                                </td>
                                <td>
                                    <c:out value="${employee.getLastname()}" />
                                </td>
                                <td>
                                    <c:out value="${employee.getEmail()}" />
                                </td>
                                <td>
                                    <c:out value="${employee.getDepartment().getDepartmentName()}" />
                                </td>
                                <td>
                                    <c:out value="${employee.getPosition().getPositionName()}" />
                                </td>
                                <td>
                                    <div class="btn-group float-right" role="group">
                                        <a href="#" type="button" class="btn btn-info" title="View">
                                            <i class="fas fa-search-plus"></i>
                                        </a>
                                        <a href="<%=request.getAttribute("contextPath")%>/EmployeeServlet?id=<c:out value="${employee.getId()}" />" type="button" class="btn btn-info" title="Edit">
                                            <i class="far fa-edit"></i>
                                        </a>
                                        <a href="#" type="button" class="btn btn-info" title="Delete">
                                            <i class="far fa-times-circle"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <tr><td colspan="7">Nincsenek felvéve dolgozók.</td></tr>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>
</div>
<jsp:include page="footer.jsp"></jsp:include>


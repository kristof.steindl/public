<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Department Form</title>
    </head>
    <body>
        <h1>Department form</h1>
        <form method="POST">
            Id: <input type="text" name="id" placeholder="Generated automatically" value="" disabled/>
            <br>
            Name: <input type="text" name="department_name" value="" />
            <br>
            Department Boss: <select name="employeeId">
                <c:forEach items="${employeeList}" var="item">
                    <option value="${item.getId()}"><c:out value="${item.toStringToDropDown()}" /></option>
                </c:forEach>
            </select>
            <input type="submit" value="Save" />
        </form>
    </body>
</html>

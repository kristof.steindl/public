# IndexFileMaker

The application makes an "index.txt" file of a given input in the root folder. 
For every occuring word in the input is represented by row in the index, in alphabetical order.
In the index file, after every word there is/are number(s) thich is/are the row number of the appearence in the original input text file. 
The input is a text file of ascii characters and must be in the root folder in the project with "test.txt" name.
package hu.steindl.kristof.indexfilemaker.service;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Steindl Kristof
 */
public class IndexFileMaker {
	
	public static final String INDEX_FILE_NAME = "index.txt";
	public static final List<String> SPECIAL_CHARS = Arrays.asList(new String[] {".", "!", "?", ",",":"});
	
	public void createIndexFile(String inputFilePath) {
		Map<String, Set<Integer>> indexMap = createIndexMap(inputFilePath);
		writeIndexFile(indexMap, inputFilePath);
	}
	
	private void writeIndexFile(Map<String, Set<Integer>> indexMap, String inputFilePath) {
		File inputFile = new File(inputFilePath);
		String outputFileName = inputFile.getParentFile().getAbsolutePath().concat("\\").concat(INDEX_FILE_NAME);
		System.out.println(outputFileName);
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFileName), "utf-8"))){
			indexMap.entrySet().stream().forEach(entrySet -> {
				try {
					writer.write(entrySet.getKey() + " "); 
					StringBuilder stringBuilder = new StringBuilder();
					entrySet.getValue().stream().forEach(number -> stringBuilder.append(number).append(", "));
					stringBuilder.delete(stringBuilder.length()-2, stringBuilder.length());
					writer.write(stringBuilder.toString());
					writer.write("\n");
				} catch (IOException ex) {
					Logger.getLogger(IndexFileMaker.class.getName()).log(Level.SEVERE, null, ex);
				}
				});
			} catch (FileNotFoundException ex) {
			Logger.getLogger(IndexFileMaker.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnsupportedEncodingException ex) {
			Logger.getLogger(IndexFileMaker.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(IndexFileMaker.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private Map<String, Set<Integer>> createIndexMap(String inputFilePath) {
		File file = new File(inputFilePath); 
		BufferedReader bufferedReader; 
		Map<String, Set<Integer>> indexMap = new TreeMap<>();
		try {
			bufferedReader = new BufferedReader(new FileReader(file));
			int lineNumber = 1;
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				String[] stringLine = line.split(" ");
				for (String word : stringLine) {
					String processedWord = getProcessedWord(word);
					Set<Integer> wordAppearanceList = indexMap.get(processedWord);
					if (wordAppearanceList == null) {
						wordAppearanceList = new LinkedHashSet<>();
					}
					indexMap.put(processedWord, wordAppearanceList);	
					wordAppearanceList.add(lineNumber);				
				}
				lineNumber ++;
			}
		} catch (UnsupportedEncodingException ex) { 
			Logger.getLogger(IndexFileMaker.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(IndexFileMaker.class.getName()).log(Level.SEVERE, null, ex);
		}
		finally {
			System.out.println(indexMap);
			return indexMap;
		}
	}
	
	private String getProcessedWord(String word) {
		StringBuilder stringBuilder = new StringBuilder(word.toLowerCase());
		if (SPECIAL_CHARS.contains(stringBuilder.substring(stringBuilder.length()-1))) {
			stringBuilder.deleteCharAt(stringBuilder.length()-1);
		}
		return stringBuilder.toString();
	} 
	
}

package hu.steindl.kristof.indexfilemaker;


import hu.steindl.kristof.indexfilemaker.service.IndexFileMaker;

/**
 *
 * @author Steindl Kristof
 */
public class IndexApp {
	
	public static String TEST_FILE = "./test.txt";
	
	public static void main(String[] args) {
		IndexFileMaker indexFileMaker = new IndexFileMaker();
		indexFileMaker.createIndexFile(TEST_FILE);
	}
	
}

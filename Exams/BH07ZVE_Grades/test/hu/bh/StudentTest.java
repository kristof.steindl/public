//Steindl Kristóf
//BH07
package hu.bh;

import hu.bh.entities.Grade;
import hu.bh.entities.Student;
import hu.bh.enums.ExamType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lborisz
 */
public class StudentTest {
  
  private static EntityManagerFactory emf;
  private static EntityManager em;
  private static Integer gradeId;
  private static List<Grade> grades;
  
  public StudentTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
    emf = Persistence.createEntityManagerFactory("BH07ZVE_GradesPU");
  }
  
  @AfterClass
  public static void tearDownClass() {
    emf.close();
  }
  
  @Before
  public void setUp() {
    em = emf.createEntityManager();
  }
  
  @After
  public void tearDown() {
    em.close();
  }

   @Test
   public void insertGrade() {
     Grade grade = new Grade();
     grade.setExamType(ExamType.FIRSTEXAM);
     grade.setGrade(75.67);
     em.getTransaction().begin();
     em.persist(grade);
     Integer id = grade.getId();
     em.getTransaction().commit();
     assertNotNull(id);
     gradeId = id;
   }
   
   @Test
   public void insertStudent(){
     Student student = new Student();
     student.setEmail("lacos@bh.hu");
     student.setFirstName("László");
     student.setLastName("Kálmán");
     Grade grade1 = em.find(Grade.class, new Integer(gradeId));
     grade1.setStudent(student);
     List<Grade> grades = new ArrayList<>();
     grades.add(grade1);
     initGrades(grades, student);
     student.setGradeList(grades);
     em.getTransaction().begin();
     em.persist(student);
     em.getTransaction().commit();
     List<Grade> resultList = em.createQuery
              ("SELECT g FROM Grade g WHERE g.student.email = 'lacos@bh.hu'").getResultList();
     sort(grades); sort(resultList);
     assertEquals(grades, resultList);
   }

   
  private void initGrades(List<Grade> grades, Student student){
    Grade grade2 = new Grade();
     grade2.setExamType(ExamType.SECONDEXAM);
     grade2.setGrade(83.23);
     grade2.setStudent(student);
     Grade grade3 = new Grade();
     grade3.setExamType(ExamType.FINALEXAM);
     grade3.setGrade(99.98);
     grade3.setStudent(student);
     grades.add(grade2); grades.add(grade3);
  } 
   
  private void sort(List<Grade> grades) {
    Collections.sort(grades, (o1, o2) -> {
      return o1.getExamType().compareTo(o2.getExamType());
    });
  }
}
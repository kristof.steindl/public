//Steindl Kristóf
//BH07
package hu.bh.app;

import hu.bh.entities.Grade;
import hu.bh.entities.Student;
import hu.bh.enums.ExamType;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Steindl Kristof
 */
public class App {
	
	public static EntityManagerFactory emf = Persistence.createEntityManagerFactory("BH07ZVE_GradesPU");
	
	public static void main(String[] args) {
		
		EntityManager em = emf.createEntityManager();
		
		Grade grade1 = new Grade(4.5, ExamType.FIRSTEXAM);
		Grade grade2 = new Grade(4.5, ExamType.FIRSTEXAM);
		
		Student student = new Student("k&s.com", "Kristóf", "Steindl");
		
		grade1.setStudent(student);
		grade2.setStudent(student);
		
		List<Grade> gradeList = new ArrayList<>();
		gradeList.add(grade1);
		gradeList.add(grade2);
		student.setGradeList(gradeList);
	
		em.getTransaction().begin();
		em.persist(student);		
		em.getTransaction().commit();
		em.close();
		
	}
	
}

/* 
Steindl Kristóf
BH07
*/
package hu.bh.model;


public class ErrorHolder {
	
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
}

/* 
Steindl Kristóf
BH07
*/
package hu.bh.model;


public class ResultHolder {
	
	private Operation operation;
	private double result;

	public Operation getOperation() {
		return operation;
	}

	public void setOperation(Operation operation) {
		this.operation = operation;
	}

	public double getResult() {
		return result;
	}

	public void setResult(double result) {
		this.result = result;
	}
	
}

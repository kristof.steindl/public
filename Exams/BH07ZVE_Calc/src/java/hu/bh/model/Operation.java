/* 
Steindl Kristóf
BH07
*/
package hu.bh.model;


public enum Operation {
	ADDING, SUBTRACTION, MULTIPLICATION, DIVISION, POWERING
}

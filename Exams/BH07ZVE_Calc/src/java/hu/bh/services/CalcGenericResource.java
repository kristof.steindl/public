/* 
Steindl Kristóf
BH07
*/
package hu.bh.services;

import hu.bh.exceptions.InvalidCalculation;
import hu.bh.model.ErrorHolder;
import hu.bh.model.Operation;
import hu.bh.model.ResultHolder;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

/**
 * REST Web Service
 *
 * @author Steindl Kristof
 */
@Path("generic")
public class CalcGenericResource {

	@Context
	private UriInfo context;

	public CalcGenericResource() {
	}

	
	@POST
	@Path("/calculate")
	@Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_JSON)
	public Response getRusult(
			@FormParam("operation") String operation, 
			@FormParam("op1") String op1,
			@FormParam("op2") String op2) {
		ResultHolder resultHolder = new ResultHolder();
		try {
			int n1, n2;
			try {
				n1 = Integer.parseInt(op1);
				n2 = Integer.parseInt(op2);				
			}
			catch (NumberFormatException ex) {
				throw new InvalidCalculation("Az operandusoknak egész 1000000000 és -1000000000 egész számoknak kell lenniük!");
			}
			if ("ADDING".equals(operation)) {
				double result = n1 + n2;
				resultHolder.setOperation(Operation.ADDING);
				resultHolder.setResult(result);
			} else if ("SUBTRACTION".equals(operation)) {
				double result = n1 - n2;
				resultHolder.setOperation(Operation.SUBTRACTION);
				resultHolder.setResult(result);
			} else if ("MULTIPLICATION".equals(operation)) {
				double result = n1 * n2;
				resultHolder.setOperation(Operation.MULTIPLICATION);
				resultHolder.setResult(result);
			} else if ("DIVISION".equals(operation)) {
				if (n2 == 0) {
					throw new InvalidCalculation("Az osztandó nem lehet 0.");
				}
				double result = n1 / n2;
				resultHolder.setOperation(Operation.DIVISION);
				resultHolder.setResult(result);
			} else if ("POWERING".equals(operation)) {
				double result = Math.pow(n1, n2);
				resultHolder.setOperation(Operation.POWERING);
				resultHolder.setResult(result);
			}
			else {
				throw new InvalidCalculation("Nem megfelelő operandus. Válassz az alábbiakből: ADDING, SUBTRACTION, MULTIPLICATION, DIVISION, POWERING.");
			}
			return Response.ok(resultHolder).build();
		} catch (Exception ex) {
			ResponseBuilder response = Response.status(Response.Status.UNAUTHORIZED) ;
			response.type(ex.getMessage());
			return response.build();
		}

	}
	
	
	@GET
    @Produces(MediaType.APPLICATION_XML)
	public String getXml() {
		//TODO return proper representation object
		throw new UnsupportedOperationException();
	}

	@PUT
    @Consumes(MediaType.APPLICATION_XML)
	public void putXml(String content) {
	}
}

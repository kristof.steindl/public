/* 
Steindl Kristóf
BH07
*/
package hu.bh.exceptions;

/**
 *
 * @author Steindl Kristof
 */
public class InvalidCalculation extends Exception{

	public InvalidCalculation() {
	}

	public InvalidCalculation(String message) {
		super(message);
	}
	
	
}

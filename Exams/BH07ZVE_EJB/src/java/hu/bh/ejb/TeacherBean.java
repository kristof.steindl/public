//Steindl Kristóf
//BH07
package hu.bh.ejb;

import hu.bh.entities.Grade;
import hu.bh.entities.Student;
import hu.bh.enums.ExamType;
import hu.bh.service.TeacherBeanService;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Remove;


@Stateful
@Remote(TeacherBeanService.class)
@LocalBean
public class TeacherBean implements TeacherBeanService{

	@EJB
	GradeManager gradeManager;
	
	private List<String> gradeStringList = new ArrayList<>();
	
	@Override
	public void saveStudent(String email, String firstName, String lastName) {
		Student student = new Student(email, firstName, lastName);
		gradeManager.saveStudent(student);
	}

	@Override
	public void saveGrade(double doubleGrade, ExamType examType, String email) {
		Grade grade = new Grade();
		grade.setGrade(doubleGrade);
		grade.setExamType(examType);
		grade.setStudent(gradeManager.findStudent(email));
		gradeManager.saveGrade(grade);
		gradeStringList.add(grade.toString());
	}
	
	
	@Remove
	@Override
	public List<String> getAllGrades() {
		return gradeStringList;
	}

	
	
}

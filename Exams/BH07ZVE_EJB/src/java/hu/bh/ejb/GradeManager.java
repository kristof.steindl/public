//Steindl Kristóf
//BH07
package hu.bh.ejb;

import hu.bh.entities.Grade;
import hu.bh.entities.Student;
import hu.bh.service.GradeManagerService;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Steindl Kristof
 */
@Stateless
@LocalBean
public class GradeManager implements GradeManagerService{

	
    @PersistenceContext(unitName = "BH07ZVE_EJBPU")
    private EntityManager em;
	
	@Override
	public void saveStudent(Student student) {
		em.persist(student);
	}

	@Override
	public void saveGrade(Grade grade) {
		em.persist(grade);
	}
	
	public Student findStudent(String email) {
		return em.find(Student.class, email);
	}
}

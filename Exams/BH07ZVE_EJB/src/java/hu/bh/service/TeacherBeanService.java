//Steindl Kristóf
//BH07
package hu.bh.service;

import hu.bh.enums.ExamType;
import java.util.List;

public interface TeacherBeanService {
	
	public void saveStudent(String email, String firstName, String lastName);	
	public void saveGrade(double doubleGrade, ExamType examype, String email);
	public List<String> getAllGrades();
	
}

//Steindl Kristóf
//BH07
package hu.bh.service;

import hu.bh.entities.Grade;
import hu.bh.entities.Student;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public interface GradeManagerService {
		
	public void saveStudent(Student student);	
	public void saveGrade(Grade grade);
	
}

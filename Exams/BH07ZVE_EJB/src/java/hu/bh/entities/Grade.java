//Steindl Kristóf
//BH07
package hu.bh.entities;

import hu.bh.enums.ExamType;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 *
 * @author Steindl Kristof
 */
@Entity
public class Grade implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private Double grade;
	
	@Enumerated(EnumType.ORDINAL)
	private ExamType examType;
	
	@JoinColumn(referencedColumnName = "EMAIL")
    @OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	private Student student;

	public Grade() {
	}

	public Grade(Double grade, ExamType examType) {
		this.grade = grade;
		this.examType = examType;
	}

	public Grade(Double grade, ExamType examType, Student student) {
		this.grade = grade;
		this.examType = examType;
		this.student = student;
	}

	public Grade(Integer id, Double grade, ExamType examType, Student student) {
		this.id = id;
		this.grade = grade;
		this.examType = examType;
		this.student = student;
	}

	public Double getGrade() {
		return grade;
	}

	public void setGrade(Double grade) {
		this.grade = grade;
	}

	public ExamType getExamType() {
		return examType;
	}

	public void setExamType(ExamType examType) {
		this.examType = examType;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Grade)) {
			return false;
		}
		Grade other = (Grade) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Grade{" + "id=" + id + ", grade=" + grade + ", examType=" + examType + ", student=" + student.getFirstName() + " " + student.getLastName();
	}

	
	
}

//Steindl Kristóf
//BH07
package hu.bh.app;

import hu.bh.enums.ExamType;
import hu.bh.service.TeacherBeanService;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class App {
	
	public static void main(String[] args) {
		try {
			Context context = null;
			context = new InitialContext();
			TeacherBeanService teacher = (TeacherBeanService) context.lookup(
					"java:global/BH07ZVE_EJB/TeacherBean!hu.bh.service.TeacherBeanService");
			
			System.out.println(teacher);
			
			teacher.saveStudent("a@b.com", "Steindl", "Kristóf");
			teacher.saveGrade(99.5, ExamType.FIRSTEXAM, "a@b.com");
			teacher.saveGrade(98.5, ExamType.SECONDEXAM, "a@b.com");
			teacher.saveGrade(97.5, ExamType.FINALEXAM, "a@b.com");
			teacher.saveGrade(97.5, ExamType.FIRSTEXAM, "lacos@bh.hu");
			
			System.out.println(teacher.getAllGrades());
		} catch (NamingException ex) {
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
		}
		
	}
	
}

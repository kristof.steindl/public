# contacts

This is project's purpuse is to demonstrate the basics of my back end knowledge.
This is a Spring Boot application, that handles contacts, it implements the CRUD operations. 
It has a H2 inmemory database, and run by embedded Tomcat. 
It has repository classes for the database operations, service classes that are responsible for the business classes.
I run the app from my IDE (Netbeans) with run command, but can be managed many ways: https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-running-your-application.html
The app is available in browser, at http://localhost:8080/web/contacts. It is Thymeleaf-html page, which is rendered and served by a Spring WebController.

REST API is available too, and can be tested by Postman (https://www.getpostman.com/) and has the folowwing signature:

GET: http://localhost:8080/rest/contacts
It returns an array of the contacts (in application json format)

GET:http://localhost:8080/rest/contacts/{id}
It returns the contact (in application json format) related to the id. If there is no contact with the Id, bad request is sent as response.

POST:http://localhost:8080/rest/contacts
It creates a new contact. The request body should be the new contact (in application json format), for example:
{
    "name": "Very new contact",
    "phone": "1234679",
    "email": "bla@bla.com"
}
If one of the attribute is missing or empty, bad request is sent as a response, and the new contact isn't persisted.

PUT:http://localhost:8080/rest/contacts
It updates a contact. The request body should be the updated contact (in application json format), for example:
{
	"id": 4,
    "name": "Very new contact",
    "phone": "1234679",
    "email": "bla@bla.com"
}
If one of the attribute is missing or empty, or there is no corresponding id, bad request is sent as a response, and the new contact isn't persisted.

DELETE:http://localhost:8080/rest/contacts/{id}
It deletes the contact

REST API can be tested by Postman (https://www.getpostman.com/).

/**
 * Author:  Steindl Kristof 
 * Created: Sep 3, 2019
 */

INSERT INTO CONTACT (FULL_NAME, PHONE, EMAIL) VALUES ('Kristof Steindl', '+36204193199', 'kristof.steindl@gmail.com');
INSERT INTO CONTACT (FULL_NAME, PHONE, EMAIL) VALUES ('David Pasztor', '+36-20-123-456', 'david.pasztor@uxstudioteam.com');


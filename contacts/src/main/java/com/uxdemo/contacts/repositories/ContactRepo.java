package com.uxdemo.contacts.repositories;

import com.uxdemo.contacts.entities.Contact;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Steindl Kristof
 */
public interface ContactRepo extends JpaRepository<Contact, Long>, CrudRepository<Contact, Long>{
	
}

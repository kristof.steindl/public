package com.uxdemo.contacts.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *Users can create, edit, delete and list their contacts. Each contact has a name, 
 * phone number and email address stored in a database. 
 * Build a very simple frontend, which can call the API to display and edit the data.
 * @author Steindl Kristof
 */


@Entity
@XmlRootElement
public class Contact implements Serializable{

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private Long id;
	private String fullName;
	private String phone; //String is for accepting every input (+36, 123-456, 123 456 etc) furthur validation is required, BE and/or FE side
	private String email;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return fullName;
	}

	public void setName(String name) {
		this.fullName = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uxdemo.contacts.controllers;

import com.uxdemo.contacts.dtos.ContactDTO;
import com.uxdemo.contacts.exceptions.GeneralContactException;
import com.uxdemo.contacts.services.ContactService;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 *
 * @author Steindl Kristof
 */
@Controller
@RequestMapping(value = "web/contacts")
public class ContactController {

	@Autowired
	ContactService contactService;

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String displayContacts() {
		return "index.html";
	}
	
	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public String helloWorldTest() {
		return "hello.html";
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String getAllContacts(Model model) {
		model.addAttribute("contactDTO",new ContactDTO());
		return prepareIndex(model);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String fillFormContacts(@PathVariable long id, Model model) {
		ContactDTO contactDTO;
		try {
			contactDTO = contactService.findById(id);
		} catch (GeneralContactException ex) {
			model.addAttribute("error", ex.getMessage());
			return "error";
		}
		model.addAttribute("contactDTO", contactDTO);
		return prepareIndex(model);
	}
	
	//deleting with GET http verb is antipattern, but DELETE verb is not supported, and thymeleaf link send a GET request, 
	//so it is the simplest (but not the most elegant) was
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String deleteContactLink(@PathVariable long id, Model model) {
		try {
			contactService.deleteContact(id);
		} catch (GeneralContactException ex) {
			model.addAttribute("error", ex.getMessage());
			return "error";
		}
		model.addAttribute("contactDTO", new ContactDTO());
		return prepareIndex(model);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String addContact(@Valid ContactDTO contactDTO, BindingResult result, Model model) {
		try {
			contactService.addContact(contactDTO);
		} catch (GeneralContactException ex) {
			model.addAttribute("error", ex.toString());
			return "error";
		}
		model.addAttribute("contactDTO", new ContactDTO());
		return prepareIndex(model);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public String updateContact(@Valid ContactDTO contactDTO, BindingResult result, Model model) {
		try {
			contactService.updateContact(contactDTO);
			return prepareIndex(model);
		} catch (GeneralContactException ex) {
			model.addAttribute("error", ex.getMessage());
			return "error";
		}
	}
	
	@RequestMapping(value = "/error", method = RequestMethod.GET)
	public String tryError(Model model) {
			model.addAttribute("error", "Try error template");
			return "error";
	}
	
	private String prepareIndex(Model model) {
		List<ContactDTO> contacts;
		try {
			contacts = contactService.findAll();
			model.addAttribute("contacts", contacts);
			if (!model.containsAttribute("contactDTO")) {
				model.addAttribute("error", new ContactDTO());
			}
			return "contacts_page";
		} catch (GeneralContactException ex) {
			// Not ideal error handling, but this is the easiest was to manage for front end. Refactoring needed!!!
			model.addAttribute("error", ex.getMessage());
			model.addAttribute("contacts", null);
			return "contacts_page";
			//return "error";
		}
	}
	
}



package com.uxdemo.contacts.rest;

import com.uxdemo.contacts.dtos.ContactDTO;
import com.uxdemo.contacts.exceptions.GeneralContactException;
import com.uxdemo.contacts.services.ContactService;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Steindl Kristof
 */
@CrossOrigin
@RestController
@RequestMapping(value = "rest/contacts")
public class ContactRestController {

	@Autowired
	ContactService contactService;

	@RequestMapping(
			method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE)
    public ContactDTO addContact(@RequestBody ContactDTO contactDTO) {
		return contactService.addContact(contactDTO);
    }
	
	@RequestMapping( 
			method = RequestMethod.PUT,
			consumes = MediaType.APPLICATION_JSON_VALUE)
    public ContactDTO updateContact(@RequestBody ContactDTO contactDTO) {
		return contactService.updateContact(contactDTO);
    }
	
	@RequestMapping("/{id}")
    public ContactDTO getAlterContactDTO(@PathVariable long id) {
		return contactService.findById(id);
    }

	@RequestMapping(
			method = RequestMethod.GET)	
    public ResponseEntity<Object> getAllContactDTO() {
		try {
			return new ResponseEntity<>(contactService.findAll(), HttpStatus.OK);
		} catch (GeneralContactException ex) {
			return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
		}
    }
	
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteContactDTO(@PathVariable long id) {
		try {
			contactService.deleteContact(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (GeneralContactException ex) {
			return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
		}
    }
}

package com.uxdemo.contacts.dtos;

import java.io.Serializable;

/**
 *
 * @author Steindl Kristof
 */
public class ContactDTO implements Serializable{
	
	private Long id;
	private String name;
	private String phone; //String is for accepting every input (+36, 123-456, 123 456 etc) furthur validation is required, BE and/or FE side
	private String email;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}

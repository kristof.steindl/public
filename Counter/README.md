# Counter

The application decides for multiple sets of numbers, if the sets fit a certain criteria or not.
The criteria for a numbr set is the following: There should be at least one, continously neighbouring subset of numbers, wwhich sum is 0.
For example:
Set1: 5,3,4,-7,9
This fits the criteria, because 3+4-7=0
Set2: 2,5,-6,8,-1
This doens't fit the criteria 
Set3:-3,-5,0,7
This fits the criteria, because 0=0 

The input file must be in the root folder of the project with the name "test.in".
The first row of the input file is the number of the sets of numbers (test cases) of the input. 
After that, two types of rows alternate as many times, as many test cases we have.
1 A number, the size of the number set. 
2 The number set itself separated by whitespaces.

The application generates an output file into the root folder of the project with the name "example.out".
Every row of the output is a "yes" or a "no" due to decision.
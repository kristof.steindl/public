package hu.steindl.kristof.counter.service;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Steindl Kristof
 */
public class Counter {
	
	private static final String OUTPUT_FILE_NAME = "example.out";
	
	public static void countWords(String inputFilePath) {
		File inputFile = new File(inputFilePath); 
		BufferedReader bufferedReader; 
		String outputFileName = inputFile.getParentFile().getAbsolutePath().concat("\\").concat(OUTPUT_FILE_NAME);
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
              new FileOutputStream(outputFileName), "utf-8"))){
			bufferedReader = new BufferedReader(new FileReader(inputFile));
			int numberOfTestCases = Integer.parseInt(bufferedReader.readLine());
			for (int i = 0; i < numberOfTestCases; i++) {
				int count = Integer.parseInt(bufferedReader.readLine());
				String line = bufferedReader.readLine();
				writer.write(getClassificationOfLine(line, count) ? "yes\n" : "no\n");
			}
		} catch (FileNotFoundException ex) {
			Logger.getLogger(Counter.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(Counter.class.getName()).log(Level.SEVERE, null, ex);
		} catch (NumberFormatException ex) {
			System.out.println("Wrong number format!");
		}
	}
	
	public static void printOutFile(String filePath) {
		File file = new File(filePath); 
		BufferedReader bufferedReader; 
		try {
			bufferedReader = new BufferedReader(new FileReader(file));
			String st; 
			while ((st = bufferedReader.readLine()) != null) {
				System.out.println(st);
			}
		} catch (FileNotFoundException ex) {
			Logger.getLogger(Counter.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(Counter.class.getName()).log(Level.SEVERE, null, ex);
		}		
	}
	
	private static boolean getClassificationOfLine(String line, int size) {
		List<Integer> numbersInLine = getIntegerList(line);
		Boolean isGood = false;
		int i = 0;
		do {
			isGood = getClassificationFromIndex(numbersInLine, i);
			i++;
		} while (i < size && !isGood);
		return isGood;
	}
	
	private static boolean getClassificationFromIndex(List<Integer> numbers, int startIndex) {
		int sum = 0;
		int i = startIndex;
		do {
			sum += numbers.get(i);
			i++;
		}
		while (i < numbers.size() && sum != 0);
		return sum == 0;
	}
	
	private static List<Integer> getIntegerList(String line) {
		String stringNumbers[] = line.split(" ");
		return Arrays.stream(stringNumbers).map(stringNumber -> Integer.parseInt(stringNumber)).collect(Collectors.toList());
	}
	
}

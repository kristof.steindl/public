package hu.steindl.kristof.counter;

import hu.steindl.kristof.counter.service.Counter;

/**
 *
 * @author Steindl Kristof
 */
public class CounterApp {
	
	public static final String INPUT_FILE_PATH = "./test.in";
	public static final String OUTPUT_FILE_PATH = "./example.out";
	
	public static void main(String[] args) {
		Counter.countWords(INPUT_FILE_PATH);
		Counter.printOutFile(OUTPUT_FILE_PATH);
		
	}
	
}
